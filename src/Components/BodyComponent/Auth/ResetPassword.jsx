import React, { useState } from 'react'
import { Grid, Paper, Typography, Link, Hidden, TextField, FormHelperText, Button, Snackbar } from '@material-ui/core'
import {
    InputAdornment
} from "@material-ui/core";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from "@material-ui/core/IconButton";
import * as Yup from "yup";
import { useFormik } from "formik";
import { userPasswordReset } from "./helper/index"
import {useParams } from 'react-router-dom'

const ResetPassword = () => {
    const { id, token } = useParams()
    const [success, setSuccess] = useState(false);
    const [message, setMessage] = useState("");
    const [state, setState] = React.useState({
        vertical: "top",
        horizontal: "right",
    });
    const { vertical, horizontal } = state;
    const [showPassword, setShowPassword] = useState(false);
    const handleClick = () => {
        setShowPassword(prev => !prev);
    }
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);
    const handleClickConfirm = () => {
        setShowConfirmPassword(prev => !prev);
    }
    const formik = useFormik({
        initialValues: {
            password: "",
            password_confirmation: "",
        },
        validationSchema: Yup.object({
            password: Yup.string().required("No password provided"),
            password_confirmation: Yup.string()
                .required("This field is required")
                .oneOf([Yup.ref("password"), null], "Passwords must match"),
        }),
        onSubmit: (values) => {
            userPasswordReset({ password: values.password, password_confirmation: values.password_confirmation }, id, token)
                .then((data) => {
                    if (data.error) {
                        setState({ vertical: "bottom", horizontal: "left" });
                        setSuccess(true);
                        setMessage(`${data.error.message}`);
                    } else {
                        if (data) {
                            values.password = "";
                            values.password_confirmation = ""
                        }
                        setMessage(`${data.message}`);
                        setState({ vertical: "bottom", horizontal: "left" });
                        setSuccess(true);
                    }
                })
                .catch((err) => console.log("Email sending is failed", err));
        },
    });
    const handleCloseSnackbar = (event) => {
        setState({ ...state });
        setSuccess(false);
    };
    const paperStyle = { padding: 20, minHeight: "30vh", width: 280, margin: "50px auto" }
    const btnstyle = { margin: "8px 0" };

    return (
        <div style={{ backgroundImage: `url("https://aawara-image-upload.s3.ap-south-1.amazonaws.com/MY_SNAPS2.png")`, height: "100vh", backgroundSize: "cover", backgroundPosition: "center center", backgroundRepeat: "no-repeat" }}>
            <Hidden mdDown>

                <img src="/MY Snaps_Logo- White-01.png" alt="My Snaps Logo" style={{
                    width: "25vw",
                    position: "absolute",
                    zIndex: "10"
                }} />
            </Hidden>
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh", float: "right", marginRight: "5%" }}>

                <Paper elevation={10} style={paperStyle}>
                    <Grid align='center'>
                        <img src="/mySnaps.png" alt="Aawara Lens Logo" width={150} style={{ margin: "16px 0 -30px 0" }} />
                        <h2>Reset Password</h2>
                    </Grid>
                    <form onSubmit={formik.handleSubmit}>
                        <TextField
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            onClick={handleClick}
                                            edge="end"
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}
                            label="Password"
                            placeholder="Password"
                            type={showPassword ? 'text' : 'password'}
                            fullWidth
                            required
                            name="password"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.password}
                        />
                        {formik.touched.password && formik.errors.password && (
                            <FormHelperText
                                error
                            >{`${formik.errors.password}`}</FormHelperText>
                        )}
                        <TextField
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            onClick={handleClickConfirm}
                                            edge="end"
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}
                            label="Confirm Password"
                            placeholder="Confirm Password"
                            type={showConfirmPassword ? 'text' : 'password'}
                            fullWidth
                            required
                            name="password_confirmation"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.password_confirmation}
                        />
                        {formik.touched.password_confirmation && formik.errors.password_confirmation && (
                            <FormHelperText
                                error
                            >{`${formik.errors.password_confirmation}`}</FormHelperText>
                        )}
                        {Object.keys(formik.errors).length === 0 ? (
                            <Button
                                type="submit"
                                variant="contained"
                                fullWidth
                                style={{
                                    btnstyle,
                                    backgroundColor: "#212121",
                                    color: "white",
                                    marginTop: "30px",
                                }}
                            >
                                Submit
                            </Button>
                        ) : (
                            <Button
                                type="submit"
                                disabled
                                variant="contained"
                                fullWidth
                                style={{
                                    btnstyle,
                                    backgroundColor: "#d6d6d6",
                                    color: "white",
                                    marginTop: "30px",
                                }}
                            >
                                Submit
                            </Button>
                        )}
                    </form>
                    <Typography align='center' style={{ marginTop: "10%" }}> Back to
                        {' '}
                        <Link href="/" >
                            Sign In
                        </Link>
                    </Typography>
                </Paper>
                <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={success}
                    message={message}
                    autoHideDuration={6000}
                    onClose={handleCloseSnackbar}
                />
            </div>
        </div>
    )
}

export default ResetPassword;