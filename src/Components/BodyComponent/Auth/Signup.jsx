import React, { useState, useEffect, useRef } from "react";
import Webcam from "react-webcam";
import {
  Grid,
  Paper,
  TextField,
  Button,
  Typography,
  Link,
  Hidden,
  FormHelperText,
  Snackbar,
  Modal,
  Box,
  InputAdornment,
} from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import { signup, getAllEEventId } from "./helper/index";
import Select from "react-select";
import Axios from "axios";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useHistory } from "react-router-dom";
import CloseIcon from "@material-ui/icons/Close";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const videoConstraints = {
  width: 220,
  height: 200,
  facingMode: "user",
};

const Signup = () => {
  const history = useHistory();
  const [photoData, setPhotoData] = useState("");
  const [state, setState] = React.useState({
    vertical: "top",
    horizontal: "right",
  });
  const { vertical, horizontal } = state;
  const [message, setMessage] = useState("");
  const [success, setSuccess] = useState(false);
  const [eventsList, setEventsList] = useState({});
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [selectedOption, setSelectedOption] = useState("");
  const [image, setImage] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const handleClick = () => {
    setShowPassword((prev) => !prev);
  };
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const handleClickConfirm = () => {
    setShowConfirmPassword((prev) => !prev);
  };
  const webcamRef = useRef(null);

  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImage(imageSrc);
    setState({ vertical: "bottom", horizontal: "left" });
    setSuccess(true);
    setMessage("Image captured successfully");
  });

  let sendData = {};

  const handleCloseSnackbar = (event) => {
    setState({ ...state });
    setSuccess(false);
  };

  useEffect(() => {
    let fetcheventList;
    fetcheventList = async () => {
      if (eventsList && Object.keys(eventsList).length === 0) {
        setEventsList(await getAllEEventId());
      }
    };
    fetcheventList();
    function create_UUID() {
      var dt = new Date().getTime();
      var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
        /[xy]/g,
        function (c) {
          var r = (dt + Math.random() * 16) % 16 | 0;
          dt = Math.floor(dt / 16);
          return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
        }
      );
      return uuid;
    }
    const fileName = create_UUID();
    const url = image;
    fetch(url)
      .then((res) => res.blob())
      .then((blob) => {
        const file = new File([blob], `${fileName}`, { type: "image/jpeg" });
        setPhotoData(file);
      });
  }, [eventsList, image]);
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  const formik = useFormik({
    initialValues: {
      firstname: "",
      lastname: "",
      email: "",
      phone: "",
      password: "",
      confirm: "",
      photo: "",
      eventid: "",
    },
    validationSchema: Yup.object({
      firstname: Yup.string().required("This field is required"),
      email: Yup.string()
        .email("Please enter a valid Email Id")
        .required("This field is required"),
      phone: Yup.string()
        .required("This field is required")
        .matches(phoneRegExp, "Phone number is not valid")
        .min(10, "to short")
        .max(10, "to long"),
      password: Yup.string().required("No password provided"),
      confirm: Yup.string()
        .required("This field is required")
        .oneOf([Yup.ref("password"), null], "Passwords must match"),
    }),
    onSubmit: (values) => {
      values.photo = photoData;

      if (selectedOption) {
        const idData = selectedOption.map((item) => {
          return item.value;
        });
        values.eventid = idData;
      } else {
        values.eventid = "";
      }
      signup({
        firstname: values.firstname,
        lastname: values.lastname,
        email: values.email,
        phone: values.phone,
        photo: values.photo,
        password: values.password,
        eventid: values.eventid,
      })
        .then((data) => {
          if (data.error) {
            setState({ vertical: "bottom", horizontal: "left" });
            setSuccess(true);
            setMessage(`${data.error}`);
          } else {
            const eventidArr = data.eventid[0].split(",");
            if (eventidArr[0].length === 0 && eventidArr.length === 1) {
            } else {
              const removeEmptyString = eventidArr.filter((e) => e);
              sendData = {
                eventid: removeEmptyString,
                photokey: data.key,
                bucketname: data.bucketname,
                photoUrl: data.photoUrl,
              };

              Axios.post(`http://192.168.1.161:2087/signupapi`, sendData, {
                "Content-Type": "application/json",
                header: "Access-Control-Allow-Origin",
              });
            }

            data = {};
            setState({ vertical: "bottom", horizontal: "left" });
            setSuccess(true);
            setMessage("Signed up successfully");
            setTimeout(() => {
              history.push("/");
            }, 3000);
          }
        })
        .catch((err) => {
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage(` Error in signup ${err}`);
        });
    },
  });

  const paperStyle = {
    padding: 20,
    minHeight: "90vh",
    height: "auto",
    width: 280,
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
    overflowY: "auto",
  };
  const btnstyle = { marginTop: "8px 0" };
  return (
    <div
      style={{
        backgroundImage: `url("MY SNAPS2.png")`,
        height: "100vh",
        backgroundSize: "cover",
        backgroundPosition: "center center",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Hidden mdDown>
        <img
          src="/MY Snaps_Logo- White-01.png"
          alt="My Snaps Logo"
          style={{
            width: "25vw",
            position: "absolute",
            zIndex: "10",
          }}
        />
      </Hidden>

      <Grid
        style={{
          height: "100vh",
          alignItems: "center",
          justifyContent: "center",
          display: "flex",
          float: "right",
          marginRight: "5%",
        }}
      >
        <Paper elevation={10} style={paperStyle}>
          <Grid align="center">
            <img
              src="/mySnaps.png"
              alt="My Snaps Logo"
              width={150}
              style={{ margin: "-30px 0px -45px" }}
            />

            <h2>Sign Up</h2>
          </Grid>
          <form onSubmit={formik.handleSubmit}>
            <TextField
              label="First Name"
              placeholder="Name"
              fullWidth
              required
              name="firstname"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.firstname}
            />
            {formik.touched.firstname && formik.errors.firstname && (
              <FormHelperText
                error
              >{`${formik.errors.firstname}`}</FormHelperText>
            )}
            <TextField
              label="Last Name"
              placeholder="Last Name"
              fullWidth
              name="lastname"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.lastname}
            />
            <TextField
              label="Whatsapp No"
              placeholder="Whatsapp No"
              fullWidth
              required
              name="phone"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.phone}
            />
            {formik.touched.phone && formik.errors.phone && (
              <FormHelperText error>{`${formik.errors.phone}`}</FormHelperText>
            )}

            <TextField
              label="Email"
              placeholder="Email"
              fullWidth
              required
              name="email"
              defaultValue=""
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.email}
            />
            {formik.touched.email && formik.errors.email && (
              <FormHelperText error>{`${formik.errors.email}`}</FormHelperText>
            )}

            {/* <input
              onChange={handleChange("photo")}
              type="file"
              name="photo"
              accept="image"
              placeholder="Upload your Image"
            /> */}
            <Button
              variant="contained"
              onClick={handleOpen}
              style={{
                backgroundColor: "#2B2B2B",
                color: "white",
                marginTop: "30px",
              }}
            >
              Take Selfie
            </Button>
            <Typography
              style={{ marginTop: "10px", marginBottom: "10px" }}
              variant="body2"
              color="textSecondary"
              component="p"
            >
              Were you available in any Event?
            </Typography>
            <Select
              defaultValue={{ value: "", label: "None" }}
              placeholder={"Event Name"}
              onChange={setSelectedOption}
              options={eventsList.options}
              name="eventid"
              isMulti
              isSearchable
            />
            <TextField
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={handleClick} edge="end">
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              label="Password"
              placeholder="Password"
              type={showPassword ? "text" : "password"}
              fullWidth
              required
              name="password"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.password}
            />
            {formik.touched.password && formik.errors.password && (
              <FormHelperText
                error
              >{`${formik.errors.password}`}</FormHelperText>
            )}
            <TextField
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={handleClickConfirm} edge="end">
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              label="Confirm Password"
              placeholder="Confirm Password"
              type={showConfirmPassword ? "text" : "password"}
              fullWidth
              required
              name="confirm"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.confirm}
            />
            {formik.touched.confirm && formik.errors.confirm && (
              <FormHelperText
                error
              >{`${formik.errors.confirm}`}</FormHelperText>
            )}

            {Object.keys(formik.errors).length === 0 ? (
              <Button
                type="submit"
                variant="contained"
                fullWidth
                style={{
                  btnstyle,
                  backgroundColor: "#212121",
                  color: "white",
                  marginTop: "30px",
                }}
              >
                Sign Up
              </Button>
            ) : (
              <Button
                type="submit"
                disabled
                variant="contained"
                fullWidth
                style={{
                  btnstyle,
                  backgroundColor: "#d6d6d6",
                  color: "white",
                  marginTop: "30px",
                }}
              >
                Sign Up
              </Button>
            )}
          </form>
          <Typography>
            {" "}
            Already have an account ?<Link href="/">Sign In</Link>
          </Typography>
        </Paper>
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={success}
          message={message}
          autoHideDuration={6000}
          onClose={handleCloseSnackbar}
        />
      </Grid>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Capture Selfie
          </Typography>
          <CloseIcon
            style={{ position: "absolute", right: 5, top: 5 }}
            onClick={handleClose}
          />
          <div className="webcam-img">
            {image === "" ? (
              <Webcam
                audio={false}
                height={220}
                ref={webcamRef}
                screenshotFormat="image/jpeg"
                width={200}
                videoConstraints={videoConstraints}
              />
            ) : (
              <img src={image} alt="" />
            )}
          </div>
          <div>
            {image !== "" ? (
              <div>
                Do you want to retake image?
                <Button
                  onClick={(e) => {
                    e.preventDefault();
                    setImage("");
                  }}
                  style={{
                    backgroundColor: "#2B2B2B",
                    color: "white",
                    marginRight: "5px",
                  }}
                  className="webcam-btn"
                >
                  Yes
                </Button>
                <Button
                  onClick={(e) => {
                    handleClose();
                  }}
                  style={{
                    backgroundColor: "#2B2B2B",
                    color: "white",
                  }}
                  className="webcam-btn"
                >
                  No
                </Button>
              </div>
            ) : (
              <Button
                onClick={(e) => {
                  e.preventDefault();
                  capture();
                }}
                style={{
                  backgroundColor: "#2B2B2B",
                  color: "white",
                  marginRight: "5px",
                }}
                className="webcam-btn"
              >
                Capture
              </Button>
            )}
          </div>
        </Box>
      </Modal>
    </div>
  );
};

export default Signup;
