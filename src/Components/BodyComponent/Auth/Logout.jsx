import React from 'react'
import { Grid, Paper, Typography, Link, Hidden } from '@material-ui/core'

const Logout = () => {

    const paperStyle = { padding: 20, minHeight: "30vh", width: 280, margin: "50px auto" }
    localStorage.removeItem("jwt")
    return (
        <div style={{ backgroundImage: `url("MY SNAPS2.png")`, height: "100vh", backgroundSize: "cover", backgroundPosition: "center center", backgroundRepeat: "no-repeat" }}>
            <Hidden mdDown>

                <img src="/MY Snaps_Logo- White-01.png" alt="My Snaps Logo" style={{
                    width: "25vw",
                    position: "absolute",
                    zIndex: "10"
                }} />
            </Hidden>
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh", float: "right", marginRight: "5%" }}>

                <Paper elevation={10} style={paperStyle}>
                    <Grid align='center'>
                        <img src="/mySnaps.png" alt="Aawara Lens Logo" width={150} style={{ margin: "16px 0 -30px 0" }} />
                        <h2>Logged out successfully</h2>
                    </Grid>
                    <Typography > Do you want to Sign In again?
                        <Link href="/" >
                            Sign In
                        </Link>
                    </Typography>
                </Paper>
            </div>
        </div>
    )
}

export default Logout;