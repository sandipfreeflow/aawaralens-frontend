import React, { useState } from 'react'
import { Grid, Paper, Typography, Link, Hidden, TextField, FormHelperText, Button, Snackbar } from '@material-ui/core'
import * as Yup from "yup";
import { useFormik } from "formik";
import { sendUserPasswordResetEmail } from "./helper/index"

const ForgotPassword = () => {
    const [success, setSuccess] = useState(false);
    const [message, setMessage] = useState("");
    const [state, setState] = React.useState({
        vertical: "top",
        horizontal: "right",
    });
    const { vertical, horizontal } = state;
    const formik = useFormik({
        initialValues: {
            email: "",
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .email("Please enter a valid Email Id")
                .required("This field is required"),
        }),
        onSubmit: (values) => {
            sendUserPasswordResetEmail({ email: values.email })
                .then((data) => {
                    console.log("dta==",data);
                    if (data.error) {
                        setState({ vertical: "bottom", horizontal: "left" });
                        setSuccess(true);
                        setMessage(`${data.error}`);
                    } else {
                        if (data) {
                            values.email = ""
                        }
                        setMessage(`${data.message}`);
                        setState({ vertical: "bottom", horizontal: "left" });
                        setSuccess(true);
                    }
                })
                .catch((err) => console.log("Email sending is failed", err));
        },
    });
    const handleCloseSnackbar = (event) => {
        setState({ ...state });
        setSuccess(false);
    };
    const paperStyle = { padding: 20, minHeight: "30vh", width: 280, margin: "50px auto" }
    const btnstyle = { margin: "8px 0" };

    return (
        <div style={{ backgroundImage: `url("MY SNAPS2.png")`, height: "100vh", backgroundSize: "cover", backgroundPosition: "center center", backgroundRepeat: "no-repeat" }}>
            <Hidden mdDown>

                <img src="/MY Snaps_Logo- White-01.png" alt="My Snaps Logo" style={{
                    width: "25vw",
                    position: "absolute",
                    zIndex: "10"
                }} />
            </Hidden>
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh", float: "right", marginRight: "5%" }}>

                <Paper elevation={10} style={paperStyle}>
                    <Grid align='center'>
                        <img src="/mySnaps.png" alt="Aawara Lens Logo" width={150} style={{ margin: "16px 0 -30px 0" }} />
                        <h2>Reset Password</h2>
                    </Grid>
                    <form onSubmit={formik.handleSubmit}>
                        <TextField
                            label="Email"
                            placeholder="Email"
                            fullWidth
                            required
                            name="email"
                            defaultValue=""
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.email}
                        />
                        {formik.touched.email && formik.errors.email && (
                            <FormHelperText error>{`${formik.errors.email}`}</FormHelperText>
                        )}
                        {Object.keys(formik.errors).length === 0 ? (
                            <Button
                                type="submit"
                                variant="contained"
                                fullWidth
                                style={{
                                    btnstyle,
                                    backgroundColor: "#212121",
                                    color: "white",
                                    marginTop: "30px",
                                }}
                            >
                                Submit
                            </Button>
                        ) : (
                            <Button
                                type="submit"
                                disabled
                                variant="contained"
                                fullWidth
                                style={{
                                    btnstyle,
                                    backgroundColor: "#d6d6d6",
                                    color: "white",
                                    marginTop: "30px",
                                }}
                            >
                                Submit
                            </Button>
                        )}
                    </form>
                    <Typography align='center' style={{ marginTop: "10%" }}> Back to
                        {' '}
                        <Link href="/" >
                            Sign In
                        </Link>
                    </Typography>
                </Paper>
                <Snackbar
                    anchorOrigin={{ vertical, horizontal }}
                    open={success}
                    message={message}
                    autoHideDuration={6000}
                    onClose={handleCloseSnackbar}
                />
            </div>
        </div>
    )
}

export default ForgotPassword;