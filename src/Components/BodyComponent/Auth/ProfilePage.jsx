import React, { useEffect, useState } from "react";
import {
    Box,
    Card,
    CardContent,
    Grid,
    Typography,
    CircularProgress,
    Paper,
    FormControl,
    Button,
    TextField,
    FormHelperText,
    Snackbar,

} from "@material-ui/core";
import { PageHeader } from "../../../Common/Components";
import { useStyles } from "../BodyStyles";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import { useFormik } from "formik";
import * as Yup from "yup";
import { motion } from "framer-motion";
import { Link, useHistory } from "react-router-dom";
import { getUserById, updateUser, isAutheticated } from "./helper";

export default function ProfilePage() {
    const classes = useStyles();
    const { user } = isAutheticated();

    const [mobileOpen, setMobileOpen] = useState(false);
    const [userData, setUserData] = useState({});
    const [success, setSuccess] = useState(false);
    const [message, setMessage] = useState("");
    const [state, setState] = React.useState({
        vertical: "top",
        horizontal: "right",
    });
    const handleCloseSnackbar = (event) => {
        setState({ ...state });
        setSuccess(false);
    };
    const { vertical, horizontal } = state;
    const phoneRegExp =
        /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    const formik = useFormik({
        initialValues: {
            firstname: `${userData.firstname === undefined ? "" : userData.firstname}`,
            lastname: `${userData.lastname === undefined ? "" : userData.lastname}`,
            email: `${userData.email === undefined ? "" : userData.email}`,
            phone: `${userData.phone === undefined ? "" : userData.phone}`,
        },
        enableReinitialize: true,
        validationSchema: Yup.object({
            firstname: Yup.string().required("This field is required"),
            email: Yup.string()
                .email("Please enter a valid Email Id")
                .required("This field is required"),
            phone: Yup.string()
                .required("This field is required")
                .matches(phoneRegExp, "Phone number is not valid")
                .min(10, "to short")
                .max(10, "to long"),
        }),
        onSubmit: (values) => {
            updateUser({ firstname: values.firstname, lastname: values.lastname, phone: values.phone })
                .then((data) => {
                    if (data.error) {
                        setState({ vertical: "bottom", horizontal: "left" });
                        setSuccess(true);
                        setMessage(`${data.error}`);
                    } else {
                        setState({ vertical: "bottom", horizontal: "left" });
                        setSuccess(true);
                        setMessage("Updated successfully");
                    }
                })
                .catch((err) => console.log("signin request failed", err));
        },
    })

    useEffect(() => {
        let getUser = async () => {
            if (Object.values(userData).length === 0) {
                setUserData(await getUserById())
            }
        }
        getUser()
    }, [userData])

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const handleDrawerClose = () => {
        setMobileOpen(false);
    };

    const paperStyle = { padding: 20, minHeight: "40vh", height: 'auto', width: 280, margin: "120px auto", backgroundColor: "#EFEFEF" }
    const btnstyle = { margin: '8px 0' }
    return (
        <div>
            <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
            <Sidenav
                mobileOpen={mobileOpen}
                handleDrawerClose={handleDrawerClose}
                handleDrawerToggle={handleDrawerToggle}
            />

            <Paper elevation={10} style={paperStyle}>
                <Grid align='center'>
                    <h2>Update Your Profile</h2>
                </Grid>
                <img
                    style={{ width: "50%", height: "50%" }}
                    src={`${userData && userData.photoUrl}`}
                    alt={`${userData && userData.firstname}`}
                />
                <form onSubmit={formik.handleSubmit}>
                    <TextField
                        label="First Name"
                        placeholder="Name"
                        fullWidth
                        required
                        name="firstname"
                        defaultValue={`${Object.values(userData).length !== 0 ? userData.firstname : ""}`}
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.firstname}
                    />
                    {formik.touched.firstname && formik.errors.firstname && (
                        <FormHelperText
                            error
                        >{`${formik.errors.firstname}`}</FormHelperText>
                    )}
                    <TextField
                        label="Last Name"
                        placeholder="Last Name"
                        fullWidth
                        name="lastname"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.lastname}
                    />
                    <TextField
                        label="Whatsapp No"
                        placeholder="Whatsapp No"
                        fullWidth
                        required
                        name="phone"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.phone}
                    />
                    {formik.touched.phone && formik.errors.phone && (
                        <FormHelperText error>{`${formik.errors.phone}`}</FormHelperText>
                    )}

                    <TextField
                        label="Email"
                        placeholder="Email"
                        fullWidth
                        required
                        name="email"
                        defaultValue={`${Object.values(userData).length !== 0 ? userData.email : ""}`}
                        disabled
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.email}
                    />
                    <Button type='submit' variant="contained" fullWidth style={{
                        btnstyle,
                        backgroundColor: "#212121", color: "white", marginTop: "30px"
                    }}>Submit</Button>
                </form>

            </Paper>
            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                open={success}
                message={message}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
            />
        </div>
    );
}
