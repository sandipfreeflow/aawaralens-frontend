import { API } from "../../../../backend";

export const signup = (user) => {
  const formData = new FormData();
  for (let key in user) {
    if (key !== "eventid") formData.append(key, user[key]);
    else {
      let idArray = [];
      for (let i = 0; i < user.eventid.length; i++) {
        idArray.push(user.eventid[i]);
      }
      formData.append(key, idArray);
    }
  }
  return fetch(`${API}/signup`, {
    method: "POST",
    body: formData,
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.log(err));
};

export const getAllEEventId = async () => {
  const res = await fetch(`${API}/events/id`, {
    method: "GET",
    headers: {
      Accept: "application/json",
    },
  });
  const data = await res.json();
  return data;
};
export const signin = (user) => {
  return fetch(`${API}/signin`, {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(user),
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.log(err));
};

export const authenticate = (data, next) => {
  if (typeof window !== "undefined") {
    localStorage.setItem("jwt", JSON.stringify(data));
    next();
  }
};

export const signout = (next) => {
  if (typeof window !== "undefined") {
    localStorage.removeItem("jwt");
    next();

    return fetch(`${API}/signout`, {
      method: "GET",
    })
      .then((response) => response)
      .catch((err) => console.log(err));
  }
};

export const isAutheticated = () => {
  if (typeof window == "undefined") {
    return false;
  }
  if (localStorage.getItem("jwt")) {
    return JSON.parse(localStorage.getItem("jwt"));
  } else {
    return false;
  }
};
export const sendUserPasswordResetEmail = async (email) => {
  try {
    const response = await fetch(`${API}/sendresetpasswordemail`, {
      mode: "cors",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(email),
    });
    return await response.json();
  } catch (err) {
    return console.log(err);
  }
}
export const userPasswordReset = async (password, id, token) => {
  try {
    const response = await fetch(`${API}/userPasswordReset/${id}/${token}`, {
      mode: "cors",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(password),
    });
    return await response.json();
  } catch (err) {
    return console.log(err);
  }
}
export const getUserById = async () => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/user/${user._id}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;

}
export const updateUser = async (userDetails) => {
  console.log("userDetails", userDetails);
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));

  const res = await fetch(`${API}/user/${user._id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(userDetails),
  });

  const data = await res.json();
  return data;
}
export const changeUserPassword = async (passwordData) => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));

  try {
    const response = await fetch(`${API}/changepassword/${user._id}`, {
      mode: "cors",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,

      },
      body: JSON.stringify(passwordData),
    });
    return await response.json();
  } catch (err) {
    return console.log(err);
  }
}