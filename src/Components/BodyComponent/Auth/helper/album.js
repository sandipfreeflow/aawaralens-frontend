require("dotenv").config();
const Album = require("../models/album");
const formidable = require("formidable");
const Aws = require("aws-sdk");
const _ = require("lodash");
const fs = require("fs");
const sharp = require("sharp");

const s3 = new Aws.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID, // accessKeyId that is stored in .env file
  secretAccessKey: process.env.AWS_ACCESS_KEY_SECRET, // secretAccessKey is also stored in .env file
});

exports.getAlbumById = (req, res, next, id) => {
  Album.findById(id)
    .populate("event")
    .exec((err, album) => {
      if (err) {
        // console.log("err", err);
        return res.status(400).json({
          error: "Album not found",
        });
      }
      req.album = album;
      next();
    });
};
exports.getAllAlbumsByEventId = (req, res, id) => {
  // console.log("req", req.params.eeventId);
  // let limit = req.query.limit ? parseInt(req.query.limit) : 8;
  let sortBy = req.query.sortBy ? req.query.sortBy : "_id";
  Album.find()
    // .select("-photo")
    .populate("event")
    // .populate({
    //   path: "event",
    //   select: "name description eventDate eventVenue",
    // })
    .sort([[sortBy, "asc"]])
    .exec((err, albums) => {
      if (err) {
        // console.log(err);
        return res.status(400).json({
          error: "No album FOUND",
        });
      }
      const data = albums.filter((data) => {
        return req.params.eeventId === data.event._id.toHexString();
      });
      // console.log("data------------------>", data.length);
      if (data.length === 0) {
        return res.status(200).json([{ message: "No Album Found" }]);
      } else {
        return res.status(200).json(data);
      }
    });
};
exports.createAlbum = (req, res) => {
  let form = new formidable.IncomingForm({ multiples: true });
  form.keepExtensions = true;

  form.parse(req, (err, fields, file) => {
    if (err) {
      // console.log("err", err);
      return res.status(400).json({
        error: "problem with image",
      });
    }
    const { name, event } = fields;

    if (!name || !event) {
      return res.status(400).json({
        error: "Please include all fields",
      });
    }

    //handle file here
    // console.log("File", file);
    const photo_infos = file.photo.map((fileArrayData) => {
      const filetype = fileArrayData.mimetype.split("/");
      if (filetype[0] === "image") {
        if (fileArrayData) {
          fileArrayData.data = fs.readFileSync(fileArrayData.filepath);
          fileArrayData.contentType = fileArrayData.type;
        }
        function create_UUID() {
          var dt = new Date().getTime();
          var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
            /[xy]/g,
            function (c) {
              var r = (dt + Math.random() * 16) % 16 | 0;
              dt = Math.floor(dt / 16);
              return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
            }
          );
          return uuid;
        }

        fileArrayData.originalFilename =
          create_UUID() + fileArrayData.originalFilename;

        async function resizeImage() {
          try {
            await sharp(fileArrayData.data)
              .resize({
                width: 1008,
                height: 504,
              })
              .toBuffer()
              .then((data) => {
                const params = {
                  Bucket: process.env.AWS_COMPRESSED_BUCKET_NAME, // Storing the compressed images to s3 bucket
                  Key: fileArrayData.originalFilename, // Name of the image
                  Body: data, // Body which will contain the image in buffer format
                  ACL: "public-read-write", // defining the permissions to get the public link
                  ContentType: "image/jpeg", // Necessary to define the image content-type to view the photo in the browser with the link
                };
                // uploading the photo using s3 instance and saving the link in the database.

                s3.upload(params, (error, compressedImageData) => {
                  if (error) {
                    res.status(500).send({ err: error }); // if we get any error while uploading error message will be returned.
                  }
                });
              });
          } catch (error) {
            console.log(error);
          }
        }
        resizeImage();
        async function resizeImageToThumbnail() {
          try {
            await sharp(fileArrayData.data)
              .resize({
                width: 320,
                height: 180,
              })
              .toBuffer()
              .then((data) => {
                const params = {
                  Bucket: process.env.AWS_THUMBNAIL_BUCKET_NAME, // Storing the thumbnail images to s3 bucket
                  Key: fileArrayData.originalFilename, // Name of the image
                  Body: data, // Body which will contain the image in buffer format
                  ACL: "public-read-write", // defining the permissions to get the public link
                  ContentType: "image/jpeg", // Necessary to define the image content-type to view the photo in the browser with the link
                };
                // uploading the photo using s3 instance and saving the link in the database.

                s3.upload(params, (error, thumbnailImageData) => {
                  if (error) {
                    res.status(500).send({ err: error }); // if we get any error while uploading error message will be returned.
                  }
                });
              });
          } catch (error) {
            console.log(error);
          }
        }
        resizeImageToThumbnail();
        //save to the DB
        const params = {
          Bucket: process.env.AWS_BUCKET_NAME, // bucket that we made earlier
          Key: fileArrayData.originalFilename, // Name of the image
          Body: fileArrayData.data, // Body which will contain the image in buffer format
          ACL: "public-read-write", // defining the permissions to get the public link
          ContentType: "image/jpeg", // Necessary to define the image content-type to view the photo in the browser with the link
        };
        // uploading the photo using s3 instance and saving the link in the database.

        s3.upload(params, (error, data) => {
          if (error) {
            res.status(500).send({ err: error }); // if we get any error while uploading error message will be returned.
          }
        });
        return {
          s3Url: `s3://${process.env.AWS_BUCKET_NAME}/${fileArrayData.originalFilename}`,
          photoUrl: `https://${process.env.AWS_BUCKET_NAME}.s3.amazonaws.com/${fileArrayData.originalFilename}`,
          compressedphotoUrl: `https://${process.env.AWS_COMPRESSED_BUCKET_NAME}.s3.amazonaws.com/${fileArrayData.originalFilename}`,
          compresseds3Url: `s3://${process.env.AWS_COMPRESSED_BUCKET_NAME}/${fileArrayData.originalFilename}`,
          thumbnails3Url: `https://${process.env.AWS_THUMBNAIL_BUCKET_NAME}.s3.amazonaws.com/${fileArrayData.originalFilename}`,
          thumbnailphotoUrl: `s3://${process.env.AWS_THUMBNAIL_BUCKET_NAME}/${fileArrayData.originalFilename}`,
          keyphotoUrl: fileArrayData.originalFilename,
          keycompressedphotoUrl: fileArrayData.originalFilename,
          keythumbnailphotoUrl: fileArrayData.originalFilename,
        };
      } else {
        return {
          s3Url: undefined,
          photoUrl: undefined,
          compressedphotoUrl: undefined,
          compresseds3Url: undefined,
          thumbnails3Url: undefined,
          thumbnailphotoUrl: undefined,
          keyphotoUrl: undefined,
          keycompressedphotoUrl: undefined,
          keythumbnailphotoUrl: undefined,
        };
      }
    });

    let album = new Album(fields);

    let s3Urls = [];
    let photoUrls = [];
    let compressedphotoUrls = [];
    let compresseds3Urls = [];
    let thumbnails3Urls = [];
    let thumbnailphotoUrls = [];
    let keyphotoUrls = [];
    let keycompressedphotoUrls = [];
    let keythumbnailphotoUrls = [];

    photo_infos.map((photo_info) => {
      s3Urls.push(photo_info.s3Url);
      photoUrls.push(photo_info.photoUrl);
      compressedphotoUrls.push(photo_info.compressedphotoUrl);
      compresseds3Urls.push(photo_info.compresseds3Url);
      thumbnails3Urls.push(photo_info.thumbnails3Url);
      thumbnailphotoUrls.push(photo_info.thumbnailphotoUrl);
      keyphotoUrls.push(photo_info.keyphotoUrl);
      keycompressedphotoUrls.push(photo_info.keycompressedphotoUrl);
      keythumbnailphotoUrls.push(photo_info.keycompressedphotoUrl);
    });
    album.s3Url = s3Urls;
    album.photoUrl = photoUrls;
    album.compressedphotoUrl = compressedphotoUrls;
    album.compresseds3Url = compresseds3Urls;
    album.thumbnails3Url = thumbnails3Urls;
    album.thumbnailphotoUrl = thumbnailphotoUrls;
    album.keycompressedphotoUrl = keycompressedphotoUrls;
    album.keyphotoUrl = keyphotoUrls;
    album.keythumbnailphotoUrl = keythumbnailphotoUrls;

    album.save((err, album) => {
      if (err) {
        res.status(400).json({
          error: "Saving Album in DB failed",
          err,
        });
      }
    });
    // console.log("REsponse album", album);
    res.json(album);
  });
};
exports.getAlbum = (req, res) => {
  req.album.photo = undefined;
  return res.json(req.album);
};

exports.deleteAlbum = (req, res) => {
  let album = req.album;
  album.remove((err, deletedAlbum) => {
    if (err) {
      return res.status(400).json({
        error: "Failed to delete the Album",
      });
    }
    // console.log("deletedAlbum", deletedAlbum);
    deletedAlbum.keycompressedphotoUrl.map((keycompressedphotoUrl) => {
      console.log("keycompressedphotoUrl", keycompressedphotoUrl);

      console.log("keycompressedphotoUrl is called----jhj----------------->");

      try {
        s3.deleteObject(
          {
            Bucket: process.env.AWS_COMPRESSED_BUCKET_NAME,
            Key: `${keycompressedphotoUrl}`,
          },
          (err, data) => {
            if (err) console.log("err", err);
            else console.log("data", data);
          }
        );
      } catch (error) {
        console.log(" ===== " + error);
      }
    });
    deletedAlbum.keyphotoUrl.map((keyphotoUrl) => {
      console.log("keyphotoUrl*********************", typeof keyphotoUrl);

      console.log("deleteFromkeyphotoUrl is called--------------------->");
      s3.deleteObject(
        {
          Bucket: process.env.AWS_BUCKET_NAME,
          Key: `${keyphotoUrl}`,
        },
        (err, data) => {
          if (err) console.log("err", err);
          else console.log("data", data);
        }
      );
    });
    deletedAlbum.keythumbnailphotoUrl.map((keythumbnailphotoUrl) => {
      console.log("keythumbnailphotoUrl", keythumbnailphotoUrl);

      console.log(
        "deleteFromthumbnailphotoUrl is called--------------------->"
      );
      s3.deleteObject(
        {
          Bucket: process.env.AWS_THUMBNAIL_BUCKET_NAME,
          Key: `${keythumbnailphotoUrl}`,
        },
        (err, data) => {
          if (err) console.log("err", err);
          else console.log("data", data);
        }
      );
    });
    res.json({
      message: "Deletion was a success",
      deletedAlbum,
    });
  });
};

exports.updateAlbum = (req, res) => {
  let form = new formidable.IncomingForm();
  form.keepExtensions = true;

  form.parse(req, (err, fields, file) => {
    if (err) {
      return res.status(400).json({
        error: "problem with image",
      });
    }

    //updation code
    let album = req.album;
    album = _.extend(album, fields);

    //handle file here
    if (file.photo) {
      album.photo.data = fs.readFileSync(file.photo.path);
      album.photo.contentType = file.photo.type;
    }

    //save to the DB
    album.save((err, album) => {
      if (err) {
        res.status(400).json({
          error: "Updation of album failed",
        });
      }
      res.json(album);
    });
  });
};

exports.getAllAlbums = (req, res) => {
  // let limit = req.query.limit ? parseInt(req.query.limit) : 8;
  // let sortBy = req.query.sortBy ? req.query.sortBy : "_id";

  // console.log("limit", limit);
  // console.log("sortBy", sortBy);
  Album.find()
    .select("-photo")
    .populate("event")
    // .populate({
    //   path: "event",
    //   select: "name description eventDate eventVenue",
    // })
    // .sort([[sortBy, "asc"]])
    // .limit(limit)
    .exec((err, albums) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
          error: "No album FOUND",
        });
      }
      console.log("albums", albums);
      res.json(albums);
    });
};
exports.getAllAlbumsImages = (req, res) => {
  let galleryThumbnailUrlsArr = [];
  let galleryPhotoUrlArr = [];
  Album.find().exec((err, albums) => {
    if (err) {
      console.log(err);
      return res.status(400).json({
        error: "No album FOUND",
      });
    }
    albums.map((albumData) => {
      albumData.thumbnails3Url.map((item) =>
        galleryThumbnailUrlsArr.push(item)
      );
    });
    albums.map((albumData) => {
      albumData.photoUrl.map((item) => galleryPhotoUrlArr.push(item));
    });
    res.json({ galleryThumbnailUrlsArr, galleryPhotoUrlArr });
  });
};
exports.getAllAlbumsCount = (req, res, next) => {
  Album.find().count((err, count) => {
    if (err) {
      return res.status(400).json({
        error: "No albums found",
      });
    }
    res.status(200).json({ albumCount: count });
    next();
  });
};
