import React from 'react'
import { Grid, Paper, Typography, Link } from '@material-ui/core'

const Logout = () => {

    const paperStyle = { padding: 20, minHeight: "30vh", width: 280, margin: "50px auto" }
    localStorage.removeItem("jwt")
    return (
        <div style={{ background: "#000000", height: "100vh" }}>
            <img src="/MY Snaps_Logo- White-01.png" alt="My Snaps Logo" style={{
                width: "25vw",
                height: "50vh",
                position: "absolute",
                zIndex: "10"
            }} />
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100vh", float: "right", marginRight: "5%" }}>

                <Paper elevation={10} style={paperStyle}>
                    <Grid align='center'>
                        <img src="/mySnaps.png" alt="Aawara Lens Logo" width={150} style={{ margin: "16px 0 -30px 0" }} />
                        <h2>Logged out successfully</h2>
                    </Grid>
                    <Typography > Do you want to Sign In again?
                        <Link href="/" >
                            Sign In
                        </Link>
                    </Typography>
                </Paper>
            </div>
        </div>
    )
}

export default Logout;