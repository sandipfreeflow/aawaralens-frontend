import React, { useState } from "react";
import {
  Grid,
  Paper,
  TextField,
  Button,
  Typography,
  FormHelperText,
  Hidden,
  InputAdornment,
  Snackbar,
} from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import { Link, useHistory } from "react-router-dom";
import { signin, authenticate, isAutheticated } from "./helper/index";
import * as Yup from "yup";
import { useFormik } from "formik";

const Signin = () => {
  const { user } = isAutheticated();
  const history = useHistory();
  const didRedirect = (user) => {
    if (JSON.parse(localStorage.getItem("jwt"))) {
      if (user.role === 1) {
        history.push("/admin/dashboard");
      } else if (user.role === 0) {
        history.push("/user/events");
      } else if (user.role === 2) {
        history.push("/photographer/events");
      }
    }
  };
  didRedirect(user);
  const [showPassword, setShowPassword] = useState(false);
  const [success, setSuccess] = useState(false);
  const [message, setMessage] = useState("");
  const [state, setState] = React.useState({
    vertical: "top",
    horizontal: "right",
  });
  const { vertical, horizontal } = state;

  const handleClick = () => {
    setShowPassword((prev) => !prev);
  };
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Please enter a valid Email Id")
        .required("This field is required"),
      password: Yup.string().required("No password provided"),
      // .min(8, 'Password is too short - should be 8 chars minimum.')
      // .matches(/[a-zA-Z]/, 'Password can only contain Latin letters.')
    }),
    onSubmit: (values) => {
      signin({ email: values.email, password: values.password })
        .then((data) => {
          if (data.error) {
            setState({ vertical: "bottom", horizontal: "left" });
            setSuccess(true);
            setMessage(`${data.error}`);
          } else {
            authenticate(
              {
                ...data,
              },
              () => {
                didRedirect(data.user);
              }
            );
          }
        })
        .catch((err) => console.log("signin request failed", err));
    },
  });
  const handleCloseSnackbar = (event) => {
    setState({ ...state });
    setSuccess(false);
  };
  const paperStyle = {
    padding: 20,
    minHeight: "70vh",
    height: "auto",
    width: 280,
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
    overflowY: "auto",
  };
  const btnstyle = { margin: "8px 0" };
  return (
    <div
      style={{
        backgroundImage: `url("MY SNAPS2.png")`,
        height: "100vh",
        backgroundSize: "cover",
        backgroundPosition: "center center",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Hidden mdDown>
        <img
          src="/MY Snaps_Logo- White-01.png"
          alt="My Snaps Logo"
          style={{
            width: "25vw",
            position: "absolute",
            zIndex: "10",
          }}
        />
      </Hidden>

      <Grid
        style={{
          height: "100vh",
          alignItems: "center",
          justifyContent: "center",
          display: "flex",
          float: "right",
          marginRight: "5%",
        }}
      >
        <Paper elevation={10} style={paperStyle}>
          <Grid align="center">
            <img
              src="/mySnaps.png"
              alt="My Snaps Logo"
              width={150}
              style={{ margin: "16px 0 -30px 0" }}
            />
            <h2>Sign In</h2>
          </Grid>
          <form onSubmit={formik.handleSubmit}>
            <TextField
              label="Email"
              placeholder="Email"
              fullWidth
              required
              name="email"
              defaultValue=""
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.email}
            />
            {formik.touched.email && formik.errors.email && (
              <FormHelperText error>{`${formik.errors.email}`}</FormHelperText>
            )}
            <TextField
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={handleClick} edge="end">
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              label="Password"
              placeholder="Password"
              type={showPassword ? "text" : "password"}
              fullWidth
              required
              name="password"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.password}
            />
            {formik.touched.password && formik.errors.password && (
              <FormHelperText
                error
              >{`${formik.errors.password}`}</FormHelperText>
            )}
            {Object.keys(formik.errors).length === 0 ? (
              <Button
                type="submit"
                variant="contained"
                fullWidth
                style={{
                  btnstyle,
                  backgroundColor: "#212121",
                  color: "white",
                  marginTop: "30px",
                }}
              >
                Sign in
              </Button>
            ) : (
              <Button
                type="submit"
                disabled
                variant="contained"
                fullWidth
                style={{
                  btnstyle,
                  backgroundColor: "#d6d6d6",
                  color: "white",
                  marginTop: "30px",
                }}
              >
                Sign in
              </Button>
            )}
          </form>
          <Typography align="center" style={{ marginTop: "10%" }}>
            {" "}
            Do you have an account ?
            <Link to="/signup" style={{ textDecoration: "none" }}>
              {" "}
              Sign Up
            </Link>
          </Typography>
          <Typography align="center" style={{ marginTop: "10%" }}>
            {" "}
            <Link
              to="/sendresetpasswordemail"
              style={{ textDecoration: "none" }}
            >
              Forgot Password
            </Link>
          </Typography>
        </Paper>
      </Grid>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={success}
        message={message}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      />
    </div>
  );
};

export default Signin;
