import React, { useEffect, useState } from "react";
import {
    Box,
    Card,
    CardContent,
    Grid,
    Typography,
    CircularProgress,
    Paper,
    FormControl,
    Button,
    TextField,
    FormHelperText,
    Snackbar,
    InputAdornment
} from "@material-ui/core";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from "@material-ui/core/IconButton";
import { PageHeader } from "../../../Common/Components";
import { useStyles } from "../BodyStyles";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import { useFormik } from "formik";
import * as Yup from "yup";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";
import { changeUserPassword } from "./helper";

export default function ChangePassword() {
    const classes = useStyles();
    const [mobileOpen, setMobileOpen] = useState(false);
    const [userData, setUserData] = useState({});
    const [values, setValues] = useState({
        photo: "",
    });
    const [success, setSuccess] = useState(false);
    const [message, setMessage] = useState("");
    const [state, setState] = React.useState({
        vertical: "top",
        horizontal: "right",
    });
    const { vertical, horizontal } = state;
    const [showPassword, setShowPassword] = useState(false);
    const handleClick = () => {
        setShowPassword(prev => !prev);
    }
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);
    const handleClickConfirm = () => {
        setShowConfirmPassword(prev => !prev);
    }
    const formik = useFormik({
        initialValues: {
            password: "",
            password_confirmation: "",
        },
        validationSchema: Yup.object({
            password: Yup.string().required("No password provided"),
            password_confirmation: Yup.string()
                .required("This field is required")
                .oneOf([Yup.ref("password"), null], "Passwords must match"),
        }),
        onSubmit: (values) => {
            changeUserPassword({ password: values.password, password_confirmation: values.password_confirmation })
                .then((data) => {
                    if (data.status === 400) {
                        setState({ vertical: "bottom", horizontal: "left" });
                        setSuccess(true);
                        setMessage(`${data.message}`);
                    } else {
                        if (data) {
                            values.password = "";
                            values.password_confirmation = ""
                        }
                        setMessage(`${data.message}`);
                        setState({ vertical: "bottom", horizontal: "left" });
                        setSuccess(true);
                    }
                })
                .catch((err) => console.log("error", err));
        },
    });

    // useEffect(() => {
    //     let getUser = async () => {
    //         if (Object.values(userData).length === 0) {
    //             setUserData(await getUserById())
    //         }
    //     }
    //     getUser()
    // }, [userData])

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const handleDrawerClose = () => {
        setMobileOpen(false);
    };
    const handleCloseSnackbar = (event) => {
        setState({ ...state });
        setSuccess(false);
    };
    const paperStyle = { padding: 20, minHeight: "40vh", height: 'auto', width: 280, margin: "150px auto", backgroundColor: "#EFEFEF" }
    const btnstyle = { margin: '8px 0' }
    return (
        <div>
            <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
            <Sidenav
                mobileOpen={mobileOpen}
                handleDrawerClose={handleDrawerClose}
                handleDrawerToggle={handleDrawerToggle}
            />

            <Paper elevation={10} style={paperStyle}>
                <Grid align='center'>
                    <h2>Change your password</h2>
                </Grid>

                <form onSubmit={formik.handleSubmit}>
                    <TextField
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={handleClick}
                                        edge="end"
                                    >
                                        {showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        label="Password"
                        placeholder="Password"
                        type={showPassword ? 'text' : 'password'}
                        fullWidth
                        required
                        name="password"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.password}
                    />
                    {formik.touched.password && formik.errors.password && (
                        <FormHelperText
                            error
                        >{`${formik.errors.password}`}</FormHelperText>
                    )}
                    <TextField
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={handleClickConfirm}
                                        edge="end"
                                    >
                                        {showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        label="Confirm Password"
                        placeholder="Confirm Password"
                        type={showConfirmPassword ? 'text' : 'password'}
                        fullWidth
                        required
                        name="password_confirmation"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.password_confirmation}
                    />
                    {formik.touched.password_confirmation && formik.errors.password_confirmation && (
                        <FormHelperText
                            error
                        >{`${formik.errors.password_confirmation}`}</FormHelperText>
                    )}
                    {Object.keys(formik.errors).length === 0 ? (
                        <Button
                            type="submit"
                            variant="contained"
                            fullWidth
                            style={{
                                btnstyle,
                                backgroundColor: "#212121",
                                color: "white",
                                marginTop: "30px",
                            }}
                        >
                            Submit
                        </Button>
                    ) : (
                        <Button
                            type="submit"
                            disabled
                            variant="contained"
                            fullWidth
                            style={{
                                btnstyle,
                                backgroundColor: "#d6d6d6",
                                color: "white",
                                marginTop: "30px",
                            }}
                        >
                            Submit
                        </Button>
                    )}
                </form>

            </Paper>
            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                open={success}
                message={message}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
            />
        </div>
    );
}
