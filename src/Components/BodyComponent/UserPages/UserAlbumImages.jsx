import React, { useState, useEffect } from "react";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { Typography, Paper } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import GetAppIcon from "@material-ui/icons/GetApp";
import CloseIcon from "@material-ui/icons/Close";
import { PageHeader } from "../../../Common/Components";
import { getUserFileDetails } from "./helper/index";
import FullscreenIcon from "@material-ui/icons/Fullscreen";
import HdIcon from "@material-ui/icons/Hd";
import PhotoSizeSelectSmallIcon from "@material-ui/icons/PhotoSizeSelectSmall";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import {
  CircularProgress,
  Fab,
  Hidden,
  Menu,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@material-ui/core";
import { motion } from "framer-motion";
import moment from "moment";
import "./UserAlbumImages.css";
import Masonry from "react-masonry-css";

export default function UserAlbumImages(props) {
  const [imageLoaded, setImageLoaded] = useState(false);
  const [albumImageData, setAlbumImageData] = useState({});
  const [viewImgDetails, setViewImgDetails] = useState(false);
  const [viewCompressedImage, setViewCompressedImage] = useState("");
  const [imageName, setImageName] = useState("");
  const [captionText, setCaptionText] = useState("");
  const [captureDate, setCaptureDate] = useState("");
  const [photoDisp, setPhotoDisp] = useState("");
  const [keyCompressedUrl, setKeyCompressedUrl] = useState("");
  const [keyPhotoUrl, setKeyPhotoUrl] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [photoIndex, setPhotoIndex] = useState(0);

  const handleCloseImgDetails = () => {
    setViewImgDetails(false);
    setAnchorEl(null);
  };
  const handleClickMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setAnchorEl(null);
  };
  const compressedDownload = () => {
    download(viewCompressedImage, keyCompressedUrl);
  };
  const highRes = () => {
    download(photoDisp, keyPhotoUrl);
  };

  async function download(url, filename) {
    const a = document.createElement("a");
    a.style.display = "none";
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    handleCloseMenu();
  }
  const [mobileOpen, setMobileOpen] = useState(false);

  const dropDownData = [
    {
      label: "Compressed",
      icon: <PhotoSizeSelectSmallIcon />,
    },
    {
      label: "High Resolution",
      icon: <HdIcon />,
    },
  ];

  useEffect(() => {
    const albumFetchData = async () => {
      if (Object.values(albumImageData).length === 0) {
        setAlbumImageData(
          await getUserFileDetails(
            props.location.state.eeventId,
            props.location.state.id
          )
        );
      }
    };
    albumFetchData();
  }, [props.location.state.eeventId, props.location.state.id, albumImageData]);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };
  const viewImgDetailsStyle = {
    position: "relative",
    padding: 10,
    minHeight: "30vh",
    maxHeight: "60vh",
    overflowY: "auto",
    height: "auto",
    width: "60%",
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
    borderRadius: "20px",
  };

  const breakpointCols = {
    default: 5,
    1600: 4,
    1300: 3,
    700: 1,
  };
  return (
    <div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <PageHeader label="Album Images" title={`${props.location.state.name}`} />

      <div>
        {Object.values(albumImageData).length === 0 ? (
          <Typography
            component={"span"}
            align="center"
            style={{
              width: "100%",
              height: "calc(100vh - 175px)",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              paddingLeft: "80%",
            }}
          >
            <CircularProgress style={{ color: "black" }} />
          </Typography>
        ) : (
          <Masonry
            breakpointCols={breakpointCols}
            className="my-masonry-grid"
            columnClassName="my-masonry-grid_column"
          >
            {albumImageData.status !== 200 ? (
              <div
                style={{
                  height: "calc(100vh - 175px)",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  paddingLeft: "36%",
                }}
              >
                <h1 style={{ color: "#d3d3d3" }}>No data found</h1>
              </div>
            ) : (
              albumImageData.data.map((item, i) => (
                <div>
                  <Grid
                    lg={12}
                    key={i}
                    item
                    xs={12}
                    sm={4}
                    style={{
                      maxWidth: "400px",
                    }}
                  >
                    <motion.div whileHover={{ scale: 0.95 }}>
                      <Card
                        onClick={() => {
                          setPhotoIndex(i);
                          setViewCompressedImage(item.compressedphotourl);
                          setImageName(item.imagename);
                          setCaptionText(item.caption);
                          setCaptureDate(item.captureDate);
                          setViewImgDetails(true);
                          setPhotoDisp(item.photoUrl);
                          setKeyCompressedUrl(item.keycompressedphotoUrl);
                          setKeyPhotoUrl(item.filename);
                        }}
                        style={{
                          width: "260px",
                          borderRadius: "15px",
                          height: "250px",
                          overflow: "hidden",
                          position: "relative",
                        }}
                      >
                        <div
                        style={{
                          width: "100%",
                          height: "200px",
                          overflow: "hidden",
                          display: "block",
                        }}
                      >
                        <img
                          style={{
                            width: "100%",
                            objectFit: "cover",
                          }}
                          className={`smooth-image image-${
                            imageLoaded ? "visible" : "hidden"
                          }`}
                          onLoad={() => {
                            setImageLoaded(true);
                          }}
                          src={`${item && item.thumbnails3Url}`}
                          alt={`${item.name}`}
                        />
                        {!imageLoaded && (
                          <div className="smooth-preloader">
                            <span className="loader" />
                          </div>
                        )}
                        </div>
                        <CardContent>
                          <div
                            style={{
                              display: "flex",
                              overflow: "hidden",
                              whiteSpace: "nowrap",
                              textOverflow: "ellipsis",
                              width: "300px",
                            }}
                          >
                            {item &&
                              item.presentuser &&
                              item.presentuser.map((personName) => (
                                <Typography
                                  component={"span"}
                                  variant="subtitle1"
                                  style={{
                                    overflow: "hidden",
                                    whiteSpace: "nowrap",
                                    textOverflow: "ellipsis",
                                    width: "80px",
                                  }}
                                >
                                  {`${personName}`}
                                </Typography>
                              ))}
                          </div>
                        </CardContent>
                      </Card>
                    </motion.div>
                  </Grid>
                  <Modal
                    open={viewImgDetails}
                    onClose={handleCloseImgDetails}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <div>
                      <Paper elevation={10} style={viewImgDetailsStyle}>
                        <Hidden only={["lg", "md", "xl"]}>
                          <Fab
                            style={{
                              position: "absolute",
                              right: 15,
                              boxShadow: "none",
                            }}
                            color="inherit"
                            onClick={handleCloseImgDetails}
                            size="small"
                          >
                            <CloseIcon />
                          </Fab>
                        </Hidden>

                        <Hidden only={["sm", "xs"]}>
                          <CloseIcon
                            style={{ position: "absolute", right: 15 }}
                            onClick={handleCloseImgDetails}
                          />
                        </Hidden>
                        <Grid lg={12} container spacing={1}>
                          <Grid item lg={6} md={4} sm={12} xs={12}>
                            <img
                              style={{
                                width: "80%",
                                height: "80%",
                                objectFit:"cover",
                                borderRadius: "12px",
                                position: "relative",
                              }}
                              src={`${viewCompressedImage}`}
                              alt={`${item.compressedphotourl}`}
                            />
                            <div
                              style={{
                                display: "flex",
                              }}
                            >
                              {item &&
                                item.presentuser &&
                                item.presentuser.map((personName) => (
                                  <Typography
                                    component={"span"}
                                    variant="subtitle1"
                                    style={{ marginLeft: ".5rem" }}
                                  >
                                    {`${personName}`}
                                  </Typography>
                                ))}
                            </div>
                            <Fab
                              style={{
                                position: "absolute",
                                top: 9,
                                boxShadow: "none",
                              }}
                              color="inherit"
                              onClick={() => {
                                handleCloseImgDetails(true);
                                setIsOpen(true);
                              }}
                              size="small"
                            >
                              <FullscreenIcon
                                onClick={() => {
                                  setIsOpen(true);
                                }}
                              />
                            </Fab>
                            <Fab
                              style={{
                                position: "absolute",
                                top: 55,
                                boxShadow: "none",
                              }}
                              color="inherit"
                              onClick={(e) => {
                                handleClickMenu(e);
                              }}
                              size="small"
                            >
                              <GetAppIcon />
                            </Fab>
                          </Grid>
                          <Grid item lg={6} md={4} sm={12} xs={12}>
                            <form style={{ marginTop: "30px" }}>
                              <div className="rightdiv1">
                                <Typography
                                  component={"span"}
                                  variant="subtitle1"
                                  style={{
                                    overflow: "hidden",
                                    whiteSpace: "nowrap",
                                    textOverflow: "ellipsis",
                                    width: "300px",
                                  }}
                                >
                                  Image Name: {`${imageName}`}
                                </Typography>
                              </div>
                              <div className="rightdiv2">
                                {captionText !== "" ? (
                                  <Typography
                                    component={"span"}
                                    variant="subtitle1"
                                    style={{ marginLeft: ".5rem" }}
                                  >
                                    Caption: {`${captionText}`}
                                  </Typography>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div className="rightdiv3">
                                <Typography
                                  component={"span"}
                                  variant="subtitle1"
                                  style={{ marginLeft: ".5rem" }}
                                >
                                  Capture Date:{" "}
                                  {`${moment(captureDate)
                                    .utc()
                                    .format("YYYY-MM-DD")}`}
                                </Typography>
                              </div>
                              <div className="rightdiv4">
                                <Typography
                                  component={"span"}
                                  variant="subtitle1"
                                  style={{ marginLeft: ".5rem" }}
                                >
                                  Album Name: {`${props.location.state.name}`}
                                </Typography>
                              </div>
                            </form>
                          </Grid>
                        </Grid>
                      </Paper>
                    </div>
                  </Modal>

                  {isOpen && (
                    <Lightbox
                      mainSrc={
                        albumImageData &&
                        albumImageData.data &&
                        albumImageData.data[photoIndex] &&
                        albumImageData.data[photoIndex].photoUrl
                      }
                      nextSrc={
                        albumImageData &&
                        albumImageData.data &&
                        albumImageData.data[photoIndex + 1] &&
                        albumImageData.data[photoIndex + 1].photoUrl
                      }
                      prevSrc={
                        albumImageData &&
                        albumImageData.data &&
                        albumImageData.data[photoIndex - 1] &&
                        albumImageData.data[photoIndex - 1].photoUrl
                      }
                      onMovePrevRequest={() =>
                        setPhotoIndex(
                          (photoIndex +
                            (albumImageData &&
                              albumImageData.data &&
                              albumImageData.data.length) -
                            1) %
                            (albumImageData &&
                              albumImageData.data &&
                              albumImageData.data.length)
                        )
                      }
                      onMoveNextRequest={() =>
                        setPhotoIndex(
                          (photoIndex + 1) %
                            (albumImageData &&
                              albumImageData.data &&
                              albumImageData.data.length)
                        )
                      }
                      onCloseRequest={() => {
                        setIsOpen(false);
                        handleCloseMenu();
                      }}
                    />
                  )}
                </div>
              ))
            )}
          </Masonry>
        )}
      </div>
      <Menu
        id="profile"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleCloseMenu}
        placement="bottom-start"
        style={{ top: "80px", zIndex: 5000 }}
      >
        {dropDownData.map((item, i) => (
          <List key={i} dense={true}>
            <ListItem
              exact
              key={i}
              component={Button}
              onClick={() => {
                item.label === "Compressed" ? compressedDownload() : highRes();
              }}
            >
              <ListItemAvatar>{item.icon}</ListItemAvatar>
              <ListItemText primary={item.label}></ListItemText>
            </ListItem>
          </List>
        ))}
      </Menu>
    </div>
  );
}
