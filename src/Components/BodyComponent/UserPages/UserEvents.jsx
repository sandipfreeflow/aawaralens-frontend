import React, { useState, useEffect } from "react";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { getUserEventDetails } from "./helper/index";
import { CircularProgress, CardHeader } from "@material-ui/core";
import { motion } from "framer-motion";
import moment from "moment";
import { useHistory } from "react-router-dom";

export default function UserEvents() {
  const history = useHistory();

  const [eventData, setEventData] = useState([]);

  const [mobileOpen, setMobileOpen] = useState(false);

  useEffect(() => {
    const eventFetchData = async () => {
      if (eventData.length === 0) {
        setEventData(await getUserEventDetails());
      }
    };
    eventFetchData();
  });

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };

  return (
    <div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <h1> Events</h1>
      <Grid lg={12} container spacing={1}>
        {eventData.length <= 0 ? (
          <Typography
          component={'span'}
            align="center"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 175px)",
              paddingLeft: "45%",
            }}
          >
            <CircularProgress style={{ color: "black" }} />
          </Typography>
        ) : eventData.status !== 200 ? (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 175px)",
              paddingLeft: "36%",
            }}
          >
            <h1 style={{ color: "#d3d3d3" }}>No Event found</h1>
          </div>
        ) : (
          eventData.data.map((item, i) =>
            item.event_info.map((event) => (
              <Grid
                lg={12}
                key={i}
                item
                xs={12}
                sm={4}
                style={{ maxWidth: "300px", margin: "10px auto" }}
              >
                <motion.div whileHover={{ scale: 0.95 }}>
                  <Card
                    onClick={() => {
                      history.push(`/user/albums`, {
                        id: event._id,
                        name: event.name,
                      });
                    }}
                  >
                    <CardHeader title={event.name} />
                    <CardContent>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {event.description}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {event.eventVenue}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {moment(event.eventDate).format("YYYY-MM-DD")}
                      </Typography>
                    </CardContent>
                  </Card>
                </motion.div>
              </Grid>
            ))
          )
        )}
      </Grid>
    </div>
  );
}
