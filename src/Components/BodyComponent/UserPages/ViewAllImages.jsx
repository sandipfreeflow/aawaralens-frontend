import React, { useState, useEffect } from "react";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import GetAppIcon from "@material-ui/icons/GetApp";
import {
  CircularProgress,
  Modal,
  Paper,
  Hidden,
  Fab,
  Menu,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Button,
} from "@material-ui/core";
import { motion } from "framer-motion";
import { getSearchResult } from "./helper/index";
import FullscreenIcon from "@material-ui/icons/Fullscreen";
import HdIcon from "@material-ui/icons/Hd";
import PhotoSizeSelectSmallIcon from "@material-ui/icons/PhotoSizeSelectSmall";
import CloseIcon from "@material-ui/icons/Close";
import moment from "moment";

const ViewAllImages = () => {
  const [mobileOpen, setMobileOpen] = useState(false);
  const [searchData, setSearchData] = useState([]);
  const [photoIndex, setPhotoIndex] = useState(0);
  const [isOpen, setIsOpen] = useState(false);
  const [imageLoaded, setImageLoaded] = useState(false);
  const [viewImgDetails, setViewImgDetails] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleCloseImgDetails = () => {
    setViewImgDetails(false);
    setAnchorEl(null);
  };
  const handleClickMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setAnchorEl(null);
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };
  const dropDownData = [
    {
      label: "Compressed",
      icon: <PhotoSizeSelectSmallIcon />,
    },
    {
      label: "High Resolution",
      icon: <HdIcon />,
    },
  ];
  useEffect(() => {
    const searchResultFetchData = async () => {
      if (searchData.length === 0) {
        setSearchData(await getSearchResult());
      }
    };
    searchResultFetchData();
  }, [searchData]);
  const viewImgDetailsStyle = {
    position: "relative",
    padding: 10,
    minHeight: "30vh",
    maxHeight: "60vh",
    overflowY: "auto",
    height: "auto",
    width: "60%",
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
    borderRadius: "20px",
  };
  const compressedDownload = (viewCompressedImage, keyCompressedUrl) => {
    download(viewCompressedImage, keyCompressedUrl);
  };
  const highRes = (photoDisp, keyPhotoUrl) => {
    download(photoDisp, keyPhotoUrl);
  };

  async function download(url, filename) {
    const a = document.createElement("a");
    a.style.display = "none";
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    handleCloseMenu();
  }
  return (
    <div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />

      <Grid lg={12} container spacing={1}>
        {searchData.length === 0 ? (
          <Typography
            align="center"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 105px)",
              paddingLeft: "45%",
            }}
          >
            <CircularProgress style={{ color: "black" }} />
          </Typography>
        ) : searchData && searchData.resp && searchData.resp.status === 400 ? (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 100px)",
              paddingLeft: "35%",
            }}
          >
            <h1 style={{ color: "#d3d3d3" }}>No Image found</h1>
          </div>
        ) : (
          searchData &&
          searchData.resp &&
          searchData.resp.data &&
          searchData.resp.data.map((item, i) => (
            <div>
              <Grid
                key={i}
                item
                xs={12}
                sm={4}
                style={{
                  maxWidth: "300px",
                  minWidth: "300px",
                  margin: "10px auto",
                  padding: "10px",
                }}
              >
                <motion.div whileHover={{ scale: 0.95 }}>
                  <Card
                    onClick={() => {
                      setPhotoIndex(i);
                      setViewImgDetails(true);
                    }}
                    style={{
                      width: "260px",
                      borderRadius: "15px",
                      height: "200px",
                      overflow: "hidden",
                      position: "relative",
                    }}
                  >
                    <div
                      style={{
                        width: "100%",
                        height: "200px",
                        overflow: "hidden",
                        display: "block",
                      }}
                    >
                      <img
                        style={{ width: "100%", minHeight: "200px" }}
                        className={`smooth-image image-${
                          imageLoaded ? "visible" : "hidden"
                        }`}
                        onLoad={() => {
                          setImageLoaded(true);
                        }}
                        src={`${item.thumbnails3Url}`}
                        alt="picture123"
                      />
                      {!imageLoaded && (
                        <div className="smooth-preloader">
                          <span className="loader" />
                        </div>
                      )}
                    </div>
                  </Card>
                </motion.div>
              </Grid>
            </div>
          ))
        )}
        {isOpen && (
          <Lightbox
            mainSrc={
              searchData &&
              searchData.resp &&
              searchData.resp.data &&
              searchData.resp.data[photoIndex].photoUrl
            }
            nextSrc={
              searchData &&
              searchData.resp &&
              searchData.resp.data &&
              searchData.resp.data[photoIndex + 1] &&
              searchData.resp.data[photoIndex + 1].photoUrl
            }
            prevSrc={
              searchData &&
              searchData.resp &&
              searchData.resp.data &&
              searchData.resp.data[photoIndex - 1] &&
              searchData.resp.data[photoIndex - 1].photoUrl
            }
            onCloseRequest={() => setIsOpen(false)}
            onMovePrevRequest={() =>
              setPhotoIndex(
                (photoIndex + Object.values(searchData.resp.data).length - 1) %
                  Object.values(searchData.resp.data).length
              )
            }
            onMoveNextRequest={() =>
              setPhotoIndex(
                (photoIndex + Object.values(searchData.resp.data).length + 1) %
                  Object.values(searchData.resp.data).length
              )
            }
          />
        )}
      </Grid>
      <Modal
        open={viewImgDetails}
        onClose={handleCloseImgDetails}
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div>
          <Paper elevation={10} style={viewImgDetailsStyle}>
            <Hidden only={["lg", "md", "xl"]}>
              <Fab
                style={{
                  position: "absolute",
                  right: 15,
                  boxShadow: "none",
                }}
                color="inherit"
                onClick={handleCloseImgDetails}
                size="small"
              >
                <CloseIcon />
              </Fab>
            </Hidden>

            <Hidden only={["sm", "xs"]}>
              <CloseIcon
                style={{ position: "absolute", right: 15 }}
                onClick={handleCloseImgDetails}
              />
            </Hidden>
            <Grid lg={12} container spacing={1}>
              <Grid item lg={6} md={4} sm={12} xs={12}>
                <img
                  style={{
                    width: "80%",
                    height: "80%",
                    borderRadius: "12px",
                    position: "relative",
                  }}
                  src={`${
                    searchData &&
                    searchData.resp &&
                    searchData.resp.data &&
                    searchData.resp.data[photoIndex] &&
                    searchData.resp.data[photoIndex].compressedphotourl
                  }`}
                  alt={`${
                    searchData &&
                    searchData.resp &&
                    searchData.resp.data &&
                    searchData.resp.data[photoIndex] &&
                    searchData.resp.data[photoIndex].compressedphotourl
                  }`}
                />
                <div
                  style={{
                    display: "flex",
                  }}
                >
                  {searchData &&
                    searchData.resp &&
                    searchData.resp.data &&
                    searchData.resp.data[photoIndex] &&
                    searchData.resp.data[photoIndex].presentuser &&
                    searchData.resp.data[photoIndex].presentuser.map(
                      (personName) => (
                        <Typography
                          component={"span"}
                          variant="subtitle1"
                          style={{ marginLeft: ".5rem" }}
                        >
                          {`${personName}`}
                        </Typography>
                      )
                    )}
                </div>
                <Fab
                  style={{
                    position: "absolute",
                    top: 9,
                    boxShadow: "none",
                  }}
                  color="inherit"
                  onClick={() => {
                    handleCloseImgDetails(true);
                    setIsOpen(true);
                  }}
                  size="small"
                >
                  <FullscreenIcon
                    onClick={() => {
                      setIsOpen(true);
                    }}
                  />
                </Fab>
                <Fab
                  style={{
                    position: "absolute",
                    top: 55,
                    boxShadow: "none",
                  }}
                  color="inherit"
                  onClick={(e) => {
                    handleClickMenu(e);
                  }}
                  size="small"
                >
                  <GetAppIcon />
                </Fab>
              </Grid>
              <Grid item lg={6} md={4} sm={12} xs={12}>
                <form style={{ marginTop: "30px" }}>
                  <div className="rightdiv1">
                    <Typography
                      component={"span"}
                      variant="subtitle1"
                      style={{
                        overflow: "hidden",
                        whiteSpace: "nowrap",
                        textOverflow: "ellipsis",
                        width: "300px",
                      }}
                    >
                      Image Name:{" "}
                      {`${
                        searchData &&
                        searchData.resp &&
                        searchData.resp.data &&
                        searchData.resp.data[photoIndex] &&
                        searchData.resp.data[photoIndex].imagename
                      }`}
                    </Typography>
                  </div>
                  <div className="rightdiv2">
                    {searchData &&
                    searchData.resp &&
                    searchData.resp.data &&
                    searchData.resp.data[photoIndex] &&
                    searchData.resp.data[photoIndex].caption !== "" ? (
                      <Typography
                        component={"span"}
                        variant="subtitle1"
                        style={{ marginLeft: ".5rem" }}
                      >
                        Caption:{" "}
                        {`${
                          searchData &&
                          searchData.resp &&
                          searchData.resp.data &&
                          searchData.resp.data[photoIndex] &&
                          searchData.resp.data[photoIndex].caption
                        }`}
                      </Typography>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="rightdiv3">
                    <Typography
                      component={"span"}
                      variant="subtitle1"
                      style={{ marginLeft: ".5rem" }}
                    >
                      Capture Date:{" "}
                      {`${moment(
                        searchData &&
                          searchData.resp &&
                          searchData.resp.data &&
                          searchData.resp.data[photoIndex] &&
                          searchData.resp.data[photoIndex].captureDate
                      )
                        .utc()
                        .format("YYYY-MM-DD")}`}
                    </Typography>
                  </div>
                </form>
              </Grid>
            </Grid>
          </Paper>
        </div>
      </Modal>
      <Menu
        id="profile"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleCloseMenu}
        placement="bottom-start"
        style={{ top: "80px", zIndex: 5000 }}
      >
        {dropDownData.map((item, i) => (
          <List key={i} dense={true}>
            <ListItem
              exact
              key={i}
              component={Button}
              onClick={() => {
                item.label === "Compressed"
                  ? compressedDownload(
                      searchData.resp.data[photoIndex].compressedphotourl,

                      searchData.resp.data[photoIndex].keycompressedphotoUrl
                    )
                  : highRes(
                      searchData.resp.data[photoIndex].photoUrl,

                      searchData.resp.data[photoIndex].filename
                    );
              }}
            >
              <ListItemAvatar>{item.icon}</ListItemAvatar>
              <ListItemText primary={item.label}></ListItemText>
            </ListItem>
          </List>
        ))}
      </Menu>
    </div>
  );
};
export default ViewAllImages;
