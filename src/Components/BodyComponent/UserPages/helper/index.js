import { API } from "../../../../backend";

export const getSearchResult = async () => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/filedetailByFaceId/${user._id}`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getUserEventDetails = async () => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/usereventlist/${user._id}`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};

export const getUserAlbumDetails = async (eeventId) => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));

  const res = await fetch(`${API}/useralbumlist/${user._id}/${eeventId}`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getUserFileDetails = async (eeventId, albumId) => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));

  const res = await fetch(
    `${API}/userfilelist/${user._id}/${eeventId}/${albumId}`,
    {
      Method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  const data = await res.json();
  return data;
};
