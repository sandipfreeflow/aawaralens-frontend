import React, { useState, useEffect } from "react";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { PageHeader } from "../../../Common/Components";
import { getUserAlbumDetails } from "./helper/index";
import { CircularProgress, CardHeader } from "@material-ui/core";
import { motion } from "framer-motion";
import moment from "moment";
import { useHistory } from "react-router-dom";

export default function UserAlbums(props) {
  const history = useHistory();
  const [imageLoaded, setImageLoaded] = useState(false);
  const [albumData, setAlbumData] = useState([]);

  const [mobileOpen, setMobileOpen] = useState(false);

  useEffect(() => {
    const albumFetchData = async () => {
      if (albumData.length === 0) {
        setAlbumData(await getUserAlbumDetails(props.location.state.id));
      }
    };
    albumFetchData();
  }, [props.location.state.id, albumData]);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };

  return (
    <div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <PageHeader label="Albums" title={`${props.location.state.name}`} />

      <Grid lg={12} container spacing={1}>
        {albumData.length <= 0 ? (
          <Typography
            component="p"
            align="center"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 175px)",
              paddingLeft: "45%",
            }}
          >
            <CircularProgress style={{ color: "black" }} />
          </Typography>
        ) : albumData.status !== 200 ? (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 175px)",
              paddingLeft: "36%",
            }}
          >
            <h1 style={{ color: "#d3d3d3" }}>No data found</h1>
          </div>
        ) : (
          albumData.data.map((item, i) =>
            item.album_info.map((album) => (
              <Grid
                lg={12}
                key={i}
                item
                xs={12}
                sm={4}
                style={{ maxWidth: "300px", margin: "10px auto" }}
              >
                <motion.div whileHover={{ scale: 0.95 }}>
                  <Card
                    style={{
                      width: "260px",
                      borderRadius: "15px",
                      height: "400px",
                      overflowY: "auto",
                      position: "relative",
                    }}
                    onClick={() => {
                      history.push(`/user/albumimages`, {
                        id: album._id,
                        name: album.name,
                        eeventId: `${props.location.state.id}`,
                      });
                    }}
                  >
                    <div
                      style={{
                        width: "100%",
                        height: "200px",
                        overflow: "hidden",
                        display: "block",
                      }}
                    >
                      {album && album.photoUrl === "" ? (
                        <div
                          style={{
                            width: "100%",
                            height: "170px",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            background: "#f1f1f1",
                          }}
                        >
                          <img
                            style={{ width: "50px" }}
                            className={`smooth-image image-${
                              imageLoaded ? "visible" : "hidden"
                            }`}
                            onLoad={() => {
                              setImageLoaded(true);
                            }}
                            src="/image.svg"
                            alt={`${album.name}`}
                          />
                        </div>
                      ) : (
                        <img
                          style={{
                            width: "100%",
                            objectFit: "cover",
                          }}
                          className={`smooth-image image-${
                            imageLoaded ? "visible" : "hidden"
                          }`}
                          onLoad={() => {
                            setImageLoaded(true);
                          }}
                          src={`${album && album.photoUrl}`}
                          alt={`${album.name}`}
                        />
                      )}
                      {!imageLoaded && (
                        <div className="smooth-preloader">
                          <span className="loader" />
                        </div>
                      )}
                    </div>
                    <CardHeader title={album.name} />
                    <CardContent>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {album.description}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {album.albumVenue}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {moment(album.albumDate).format("YYYY-MM-DD")}
                      </Typography>
                    </CardContent>
                  </Card>
                </motion.div>
              </Grid>
            ))
          )
        )}
      </Grid>
    </div>
  );
}
