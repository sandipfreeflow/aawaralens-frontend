import React, { useState, useEffect } from "react";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { TextField, Paper } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import Modal from "@material-ui/core/Modal";
import CloseIcon from "@material-ui/icons/Close";
import EditIcon from "@material-ui/icons/Edit";
import Masonry from "react-masonry-css";
import "./Album.css";

import {
  createAlbum,
  updateAlbum,
  getAllAlbumsByEventID,
  deleteAlbumById,
} from "./helper/index";
import { PageHeader } from "../../../Common/Components";
import { isAutheticated } from "../Auth/helper/index";
import {
  CircularProgress,
  Fab,
  Snackbar,
  CardActions,
  Dialog,
  DialogActions,
  DialogTitle,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { motion } from "framer-motion";
import moment from "moment";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  addButton: {
    position: "fixed",
    right: 12,
    bottom: 60,
    zIndex: 99,
  },
}));

export default function Album(props) {
  const history = useHistory();
  const classes = useStyles();
  const [state, setState] = React.useState({
    vertical: "top",
    horizontal: "right",
  });
  const { vertical, horizontal } = state;
  const [values, setValues] = useState({
    name: "",
    description: "",
    event123: `${props.location.state.id}`,
    photo: "",
    albumDate: "",
    albumVenue: "",
  });
  let { name, description, photo, event123, albumDate, albumVenue } = values;
  const { user, token } = isAutheticated();
  const [open, setOpen] = useState(false);
  const [success, setSuccess] = useState(false);
  const [albumData, setAlbumData] = useState([]);
  const [openDeleteAlbum, setOpenDeleteAlbum] = useState(false);
  const handleOpen = (id, description, albumDate, albumVenue, name, photo) => {
    setOpen(true);
    setValues({
      ...values,
      _id: id,
      description: description,
      albumDate: albumDate,
      albumVenue: albumVenue,
      name: name,
      photo: photo,
    });
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [loader, setLoader] = useState(false);
  const [mobileOpen, setMobileOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [dispForm, setDispForm] = useState(1);
  const [imageLoaded, setImageLoaded] = useState(false);
  const [changeMsg, setChangeMsg] = useState(0);
  const [albumIdData, setAlbumIdData] = useState("");

  useEffect(() => {
    const albumFetchData = async () => {
      if (albumData.length === 0) {
        setAlbumData(await getAllAlbumsByEventID(props.location.state.id));
      }
    };
    albumFetchData();
  }, [props.location.state.id, albumData, albumIdData]);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };
  const handleOpenDeleteAlbum = () => {
    setOpenDeleteAlbum(true);
  };
  const handleCloseDeleteAlbum = () => {
    setOpenDeleteAlbum(false);
  };
  const handleCloseDeleteAlbumAgree = () => {
    deleteAlbum(albumIdData);
    setOpenDeleteAlbum(false);
  };
  const handleChange = (name) => (event) => {
    const value =
      name === "photo"
        ? Object.values(event.target.files).map((data) => data)
        : event.target.value;
    setValues({ ...values, error: false, [name]: value });
    // setSelectedImage(value)
  };
  const handleCloseSnackbar = (event) => {
    setState({ ...state });
    setSuccess(false);
  };
  const deleteAlbum = async (albumId) => {
    await deleteAlbumById(albumId);
    setAlbumData(await getAllAlbumsByEventID(props.location.state.id));
    setState({ vertical: "bottom", horizontal: "left" });
    setSuccess(true);
    setMessage("Album deleted successfully");
  };
  const onSubmit = async (event) => {
    setLoader(true);
    setDispForm(0);
    if (loader === true) {
      <Typography
        component={"span"}
        align="center"
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: "calc(100vh - 175px)",
          paddingLeft: "45%",
        }}
      >
        <CircularProgress style={{ color: "black" }} />
      </Typography>;
    }

    event.preventDefault();
    //backend request fired
    let eevent = event123;

    if (changeMsg === 0) {
      description = description === undefined ? "" : description;
      await createAlbum(user._id, token, {
        name,
        description,
        eevent,
        albumDate,
        albumVenue,
        photo,
      }).then(async (data) => {
        if (!data.error) {
          setLoader(true);
          setAlbumData(await getAllAlbumsByEventID(props.location.state.id));
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage("Album created successfully");
          setOpen(false);
          setDispForm(1);
        } else {
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage("Error occured while Creating the Album");
          setOpen(false);
          setDispForm(1);
          // window.location.reload();
        }
      });
    } else {
      await updateAlbum(albumIdData, {
        name,
        description,
        albumDate,
        albumVenue,
        photo,
      }).then(async (data) => {
        if (data.error) {
          // console.log(data.error);
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage("Error occured while updating the Album");
        } else {
          setDispForm(1);
          setOpen(false);
          setAlbumData(await getAllAlbumsByEventID(props.location.state.id));
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage("Album updated successfully");
        }
      });
    }
  };

  const paperStyle = {
    position: "relative",
    padding: 20,
    minHeight: "40vh",
    height: "auto",
    width: 280,
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
  };
  const btnstyle = { margin: "8px 0" };
  const breakpointCols = {
    default: 5,
    1600: 4,
    1300: 3,
    700: 1,
  };

  return (
    <div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <PageHeader label="Albums" title={`${props.location.state.name}`} />
      <motion.div
        className={classes.addButton}
        initial={{ scale: 0 }}
        animate={{ scale: 1 }}
        transition={{ ease: "easeOut", duration: 2 }}
      >
        {albumData.length > 0 ? (
          <Fab
            onClick={() => {
              handleOpen();
              setChangeMsg(0);
            }}
            style={{
              backgroundColor: "#212121",
              color: "#FFFFFF",
              height: "80px",
              width: "80px",
            }}
          >
            <AddIcon />
          </Fab>
        ) : (
          ""
        )}
      </motion.div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {dispForm === 1 ? (
          <Paper elevation={10} style={paperStyle}>
            <CloseIcon
              style={{ position: "absolute", right: 5, top: 5 }}
              onClick={handleClose}
            />
            <Grid lg={12} align="center">
              {changeMsg === 0 ? (
                <h2>Create an Album</h2>
              ) : (
                <h2>Edit an Album</h2>
              )}
            </Grid>
            <TextField
              defaultValue={`${values.name === undefined ? "" : values.name}`}
              label="Album Name"
              // placeholder="Album Name"
              type="text"
              fullWidth
              required
              onChange={handleChange("name")}
            />
            <TextField
              id="standard-multiline-static"
              label="Description"
              multiline
              rows={4}
              fullWidth
              defaultValue={`${
                values.description === undefined ? "" : values.description
              }`}
              variant="standard"
              onChange={handleChange("description")}
            />
            <TextField
              defaultValue={`${
                values.albumDate === undefined
                  ? ""
                  : moment(`${values.albumDate}`).utc().format("YYYY-MM-DD")
              }`}
              label="Album Date"
              type="date"
              focused
              fullWidth
              required
              onChange={handleChange("albumDate")}
            />
            <TextField
              defaultValue={`${
                values.albumVenue === undefined ? "" : values.albumVenue
              }`}
              label="Album Venue"
              placeholder="Album Venue"
              type="text"
              fullWidth
              required
              onChange={handleChange("albumVenue")}
            />
            <input
              type="file"
              id="files"
              name="files"
              required
              onChange={handleChange("photo")}
            />
            <Button
              type="submit"
              variant="contained"
              fullWidth
              style={{
                btnstyle,
                backgroundColor: "#212121",
                color: "white",
                marginTop: "30px",
              }}
              onClick={onSubmit}
            >
              Submit
            </Button>
          </Paper>
        ) : (
          <Typography
            component={"span"}
            variant="h5"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 175px)",
            }}
          >
            Saving Album please wait ...
            <CircularProgress style={{ color: "black" }} />
          </Typography>
        )}
      </Modal>
      {albumData.length <= 0 ? (
        <Typography
          component={"span"}
          align="center"
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "calc(100vh - 175px)",
          }}
        >
          <CircularProgress style={{ color: "black" }} />
        </Typography>
      ) : (
        <Masonry
          breakpointCols={breakpointCols}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {albumData.map((item, i) =>
            item.message ? (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "calc(100vh - 175px)",
                  // paddingLeft: "36%",
                }}
              >
                <h1 style={{ color: "#d3d3d3" }}>No Album found</h1>
              </div>
            ) : (
              <div>
                <motion.div
                  className={classes.addButton}
                  initial={{ scale: 0 }}
                  animate={{ scale: 1, transition: { delay: 0.4 } }}
                >
                  <Fab
                    onClick={() => {
                      handleOpen();
                      setChangeMsg(0);
                    }}
                    style={{
                      backgroundColor: "#212121",
                      color: "#FFFFFF",
                      height: "80px",
                      width: "80px",
                    }}
                  >
                    <AddIcon />
                  </Fab>
                </motion.div>
                <div>
                  <motion.div whileHover={{ scale: 0.95 }}>
                    <Card
                      style={{
                        width: "260px",
                        borderRadius: "15px",
                        height: "430px",
                        overflowY: "auto",
                        position: "relative",
                      }}
                      onClick={() => {
                        user.role === 1
                          ? history.push(`/albumimages/`, {
                              id: item._id,
                              name: item.name,
                              eevent: item.eevent,
                              albumDate: item.albumDate,
                              albumVenue: item.albumVenue,
                            })
                          : history.push(`/photographer/albumimages`, {
                              id: item._id,
                              name: item.name,
                              eevent: item.eevent,
                              albumDate: item.albumDate,
                              albumVenue: item.albumVenue,
                            });
                      }}
                    >
                      <div
                        style={{
                          width: "100%",
                          height: "200px",
                          overflow: "hidden",
                          display: "block",
                        }}
                      >
                        {item.photoUrl !== "" ? (
                          <img
                            style={{
                              width: "100%",
                              objectFit: "cover",
                            }}
                            className={`smooth-image image-${
                              imageLoaded ? "visible" : "hidden"
                            }`}
                            onLoad={() => {
                              setImageLoaded(true);
                            }}
                            src={`${item && item.photoUrl}`}
                            alt={`${item.name}`}
                          />
                        ) : (
                          <div
                            style={{
                              width: "100%",
                              height: "170px",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              background: "#f1f1f1",
                            }}
                          >
                            <img
                              style={{ width: "50px" }}
                              className={`smooth-image image-${
                                imageLoaded ? "visible" : "hidden"
                              }`}
                              onLoad={() => {
                                setImageLoaded(true);
                              }}
                              src="/image.svg"
                              alt={`${item.name}`}
                            />
                          </div>
                        )}
                        {!imageLoaded && (
                          <div className="smooth-preloader">
                            <span className="loader" />
                          </div>
                        )}
                      </div>
                      <CardContent>
                        <h2>{`${item.name}`}</h2>

                        <Typography
                          variant="subtitle2"
                          // color="textSecondary"
                          component="p"
                        >
                          {item.description}
                        </Typography>
                        <Typography
                          variant="body2"
                          color="textSecondary"
                          component="p"
                        >
                          {item.albumVenue}
                        </Typography>
                        <Typography
                          variant="body2"
                          color="textSecondary"
                          component="p"
                        >
                          {moment(item.albumDate).format("YYYY-MM-DD")}
                        </Typography>
                      </CardContent>
                      <CardActions>
                        {user.role === 1 ? (
                          <CardActions>
                            <EditIcon
                              onClick={(e) => {
                                e.stopPropagation();
                                setChangeMsg(1);
                                setAlbumIdData(item._id);
                                handleOpen(
                                  item._id,
                                  item.description,
                                  item.albumDate,
                                  item.albumVenue,
                                  item.name,
                                  item.photo
                                );
                              }}
                            />
                          </CardActions>
                        ) : (
                          ""
                        )}
                        {user.role === 1 ? (
                          <DeleteIcon
                            onClick={(e) => {
                              e.stopPropagation();
                              handleOpenDeleteAlbum();
                              setAlbumIdData(item._id);
                            }}
                          />
                        ) : (
                          ""
                        )}
                      </CardActions>
                    </Card>
                  </motion.div>
                </div>
              </div>
            )
          )}
        </Masonry>
      )}
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={success}
        message={message}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      />
      <Dialog
        open={openDeleteAlbum}
        onClose={handleCloseDeleteAlbum}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure want to delete the Album?"}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseDeleteAlbum}>Disagree</Button>
          <Button onClick={handleCloseDeleteAlbumAgree} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
