import { API } from "../../../../backend";
import imageCompression from "browser-image-compression";

const compressedFile = (file) => {
  const options = {
    maxSizeMB: 1,
    maxWidthOrHeight: 1000,
    useWebWorker: true,
  };
  return imageCompression(file, options);
};

export const userCount = async () => {
  const res = await fetch(`${API}/totalUser`);
  const data = await res.json();
  return await data;
};

export const eventCount = async () => {
  const res = await fetch(`${API}/totalEvent`);
  const data = await res.json();
  return await data;
};

export const albumCount = async () => {
  const res = await fetch(`${API}/totalAlbum`);
  const data = await res.json();
  return await data;
};
export const getAllFilesCount = async () => {
  const res = await fetch(`${API}/filedetailcount`);
  const data = await res.json();
  return await data;
};

export const createEvent = (userId, token, event) => {
  return fetch(`${API}/event/create/${userId}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(event),
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.log(err));
};

export const updateEEvent = async (eventId, userId, token, event) => {
  return fetch(`${API}/event/${eventId}/${userId}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(event),
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.log(err));
};

export const getAllEvents = async () => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/events`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getAllEEventByPhotographerId = async () => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/events/photographerId/${user._id}`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};

export const createAlbum = async (userId, token, album) => {
  const formData = new FormData();
  for (let key in album) {
    if (key !== "photo") formData.append(key, album[key]);
    else {
      if (album.photo !== undefined && album.photo.length > 0) {
        formData.append(
          key,
          new File(
            [await compressedFile(album.photo[0])],
            `${album.photo[0].name}`
          )
        );
      }
    }
  }

  return await fetch(`${API}/album/create/${userId}`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: formData,
  });
  // .then((response) => {
  //   return response.json();
  // })
  // .catch((err) => console.log(err));
};

export const getAllAlbums = async () => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/albums`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getAllAlbumsImages = async () => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/allalbumsimages/`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await res.json();
  return data;
};
export const getAllAlbumsByEventID = async (eventID) => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  try {
    const res = await fetch(`${API}/albums/${eventID}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await res.json();
    return data;
  } catch (e) {
    console.log(e);
  }
};

export const deleteAlbumById = async (albumId) => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/album/${albumId}/${user._id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getAlbumById = async (albumId) => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/album/${albumId}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getAllUsers = async () => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/allusers`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getAllPhotographers = async () => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/allphotographers`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getAllHosts = async () => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/allhosts`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};

export const editUserById = async (token, userId, roleData) => {
  const res = await fetch(`${API}/user/${userId}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(roleData),
  });

  const data = await res.json();
  return data;
};

export const getAllFiles = async () => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/filedetail`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const getAllImages = async () => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/getallimages`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await res.json();
  return data;
};
export const getAllFilesByAlbumId = async (id) => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/filedetail/${id}`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await res.json();

  return data;
};
export const getAllFilesByAlbumIdCount = async (id) => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/filedetailcount/${id}`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await res.json();
  return data;
};
export const updateAlbum = async (albumId, album) => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));
  const formData = new FormData();
  for (let key in album) {
    if (key !== "photo") formData.append(key, album[key]);
    else {
      if (album.photo !== undefined && album.photo.length > 0) {
        formData.append(
          key,
          new File(
            [await compressedFile(album.photo[0])],
            `${album.photo[0].name}`
          )
        );
      }
    }
  }

  return fetch(`${API}/album/${albumId}/${user._id}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: formData,
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.log(err));
};

export const fileDetailsFormEditData = async (fileId, fileData) => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/filedetailsformeditdata/${fileId}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(fileData),
  });

  const data = await res.json();
  return data;
};

export const getPersonNamesByFileId = async (fileId) => {
  const { token } = JSON.parse(localStorage.getItem("jwt"));
  const res = await fetch(`${API}/personnamebyfileid/${fileId}`, {
    Method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await res.json();
  return data;
};

export const deleteImage = async (fileId) => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));

  const res = await fetch(`${API}/deletefile/${user._id}/${fileId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
export const deleteEevent = async (eventId) => {
  const { token, user } = JSON.parse(localStorage.getItem("jwt"));

  const res = await fetch(`${API}/event/${eventId}/${user._id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const data = await res.json();
  return data;
};
