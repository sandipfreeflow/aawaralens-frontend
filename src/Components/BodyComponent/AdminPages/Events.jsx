import React, { useState, useEffect } from "react";
import Select from "react-select";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {
  TextField,
  Paper,
  Snackbar,
  Dialog,
  DialogActions,
  DialogTitle,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import Modal from "@material-ui/core/Modal";
import EditIcon from "@material-ui/icons/Edit";
import CloseIcon from "@material-ui/icons/Close";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  createEvent,
  getAllEvents,
  getAllPhotographers,
  getAllHosts,
  getAllEEventByPhotographerId,
  updateEEvent,
  deleteEevent,
} from "./helper/index";
import { isAutheticated } from "../Auth/helper/index";
import {
  CircularProgress,
  CardHeader,
  Fab,
  CardActions,
} from "@material-ui/core";
import { motion } from "framer-motion";
import moment from "moment";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  addButton: {
    position: "fixed",
    right: 12,
    bottom: 60,
    zIndex: 99,
  },
}));

export default function Events() {
  const history = useHistory();
  const classes = useStyles();
  const [state, setState] = React.useState({
    vertical: "top",
    horizontal: "right",
  });
  const { vertical, horizontal } = state;
  const [values, setValues] = useState({
    name: "",
    description: "",
    eventDate: "",
    eventVenue: "",
    host: "",
    photographer: "",
  });
  const { name, description, eventDate, eventVenue, host, photographer } =
    values;

  const { user, token } = isAutheticated();
  const [open, setOpen] = useState(false);
  const [openDeleteEvent, setOpenDeleteEvent] = useState(false);

  const [eventData, setEventData] = useState([]);
  const [photographersData, setPhotographersData] = useState([]);
  const [hostsData, setHostsData] = useState([]);
  const [success, setSuccess] = useState(false);
  const [message, setMessage] = useState("");
  const [eventIdData, setEventIdData] = useState("");
  const handleOpen = (
    id,
    description,
    eventDate,
    host,
    photographer,
    eventVenue,
    name
  ) => {
    setOpen(true);
    setValues({
      _id: id,
      description: description,
      eventDate: eventDate,
      host: host,
      photographer: photographer,
      eventVenue: eventVenue,
      name: name,
    });
  };
  const handleCloseSnackbar = (event) => {
    setState({ ...state });
    setSuccess(false);
  };
  const handleOpenDeleteEvent = () => {
    setOpenDeleteEvent(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseDeleteEvent = () => {
    setOpenDeleteEvent(false);
  };
  const handleCloseDeleteEventAgree = () => {
    deleteEvent(eventIdData);
    setOpenDeleteEvent(false);
  };
  const [mobileOpen, setMobileOpen] = useState(false);

  const [optionsPhotographer, setOptionsPhotographer] = useState([]);
  const [optionsHosts, setOptionsHost] = useState([]);
  const [changeMsg, setChangeMsg] = useState(0);

  useEffect(() => {
    let eventFetchData;
    if (user.role === 2) {
      eventFetchData = async () => {
        if (eventData.length === 0) {
          if (eventData[0] && eventData[0].message) {
            setEventData(["No data found"]);
          } else setEventData(await getAllEEventByPhotographerId());
        }
      };
    } else {
      eventFetchData = async () => {
        if (eventData.length === 0) {
          if (eventData[0] && eventData[0].message) {
            setEventData(["No data found"]);
          } else setEventData(await getAllEvents());
        }
      };
    }
    const fetchAllPhotographers = async () => {
      if (photographersData.length === 0) {
        if (photographersData[0] && photographersData[0].message) {
          setPhotographersData(["No data found"]);
        } else setPhotographersData(await getAllPhotographers());
      }
    };
    const fetchAllHosts = async () => {
      if (hostsData.length === 0) {
        if (hostsData[0] && hostsData[0].message) {
          setHostsData(["No data found"]);
        } else {
          setHostsData(await getAllHosts());
        }
      }
    };

    eventFetchData();
    fetchAllPhotographers();
    fetchAllHosts();
    const photographersList = photographersData.map((data) => {
      let optionsObject = {};
      optionsObject["value"] = data;
      optionsObject["label"] = data.firstname + " " + data.lastname;
      return optionsObject;
    });
    const hostsList = hostsData.map((data) => {
      let optionsObject = {};
      optionsObject["value"] = data;
      optionsObject["label"] = data.firstname + " " + data.lastname;
      return optionsObject;
    });
    setOptionsPhotographer(photographersList);
    setOptionsHost(hostsList);
  }, [eventData, photographersData, hostsData, user.role, eventIdData]);

  const deleteEvent = async (eventId) => {
    await deleteEevent(eventId);
    setState({ vertical: "bottom", horizontal: "left" });
    setSuccess(true);
    setMessage("Event deleted successfully");
    if (eventData[0] && eventData[0].message) {
      setEventData(["No data found"]);
    } else setEventData(await getAllEvents());
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };
  const handleSelectHost = (e) => {
    setValues({ ...values, host: e });
  };
  const handleSelectPhotographer = (e) => {
    setValues({ ...values, photographer: e });
  };
  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };
  const onSubmit = (event) => {
    event.preventDefault();
    //backend request fired
    if (changeMsg === 0) {
      createEvent(user._id, token, {
        name,
        description,
        eventDate,
        eventVenue,
        host,
        photographer,
      }).then(async (data) => {
        if (data.error) {
          console.log(data.error);
        } else {
          setOpen(false);
          setEventData(await getAllEvents());
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage("Event created successfully");
        }
      });
    } else {
      updateEEvent(eventIdData, user._id, token, {
        name,
        description,
        eventDate,
        eventVenue,
        host,
        photographer,
      }).then(async (data) => {
        if (data.error) {
          console.log(data.error);
        } else {
          setOpen(false);
          setEventData(await getAllEvents());
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage("Event updated successfully");
        }
      });
    }
  };

  const paperStyle = {
    position: "relative",
    padding: 20,
    minHeight: "40vh",
    height: "auto",
    width: 280,
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
  };
  const btnstyle = { margin: "8px 0" };
  return (
    <div>
      <motion.div
        className={classes.addButton}
        initial={{ scale: 0 }}
        animate={{ scale: 1, transition: { delay: 0.4 } }}
      >
        {user && user.role === 1 ? (
          <Fab
            style={{
              backgroundColor: "#212121",
              color: "#FFFFFF",
              height: "80px",
              width: "80px",
            }}
            onClick={() => {
              handleOpen();
              setChangeMsg(0);
            }}
          >
            <AddIcon />
          </Fab>
        ) : (
          ""
        )}
      </motion.div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <h1> Events</h1>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Paper elevation={10} style={paperStyle}>
          <CloseIcon
            style={{ position: "absolute", right: 5, top: 5 }}
            onClick={handleClose}
          />
          <Grid lg={12} align="center">
            {changeMsg === 0 ? (
              <h2>Create an Event</h2>
            ) : (
              <h2>Edit an Event</h2>
            )}
          </Grid>
          <TextField
            defaultValue={`${values.name === undefined ? "" : values.name}`}
            label="Event Name"
            placeholder="Event Name"
            type="text"
            fullWidth
            required
            onChange={handleChange("name")}
          />
          <TextField
            defaultValue={`${
              values.description === undefined ? "" : values.description
            }`}
            label="Description"
            placeholder="Description"
            type="text"
            fullWidth
            onChange={handleChange("description")}
          />
          <TextField
            defaultValue={`${
              values.eventDate === undefined
                ? ""
                : moment(`${values.eventDate}`).utc().format("YYYY-MM-DD")
            }`}
            label="Event Date"
            type="date"
            focused
            fullWidth
            required
            onChange={handleChange("eventDate")}
          />
          <TextField
            defaultValue={`${
              values.eventVenue === undefined ? "" : values.eventVenue
            }`}
            label="Event Venue"
            placeholder="Event Venue"
            type="text"
            fullWidth
            required
            onChange={handleChange("eventVenue")}
          />

          <Typography
            style={{ marginTop: "10px", marginBottom: "10px" }}
            variant="body2"
            color="textSecondary"
            component="p"
          >
            Host
          </Typography>
          <Select
            defaultValue={
              values && values.host && values.host.length === 0
                ? null
                : values.host
            }
            onChange={(val) => {
              handleSelectHost(val);
            }}
            options={optionsHosts}
            isMulti
            isSearchable
          />

          <Typography
            style={{ marginTop: "10px", marginBottom: "10px" }}
            variant="body2"
            color="textSecondary"
            component="p"
          >
            Photographer
          </Typography>
          <Select
            defaultValue={
              values && values.photographer && values.photographer.length === 0
                ? ""
                : values.photographer
            }
            onChange={(val) => {
              handleSelectPhotographer(val);
            }}
            options={optionsPhotographer}
            isMulti
            isSearchable
          />
          <Button
            type="submit"
            variant="contained"
            fullWidth
            style={{
              btnstyle,
              backgroundColor: "#212121",
              color: "white",
              marginTop: "30px",
            }}
            onClick={onSubmit}
          >
            Submit
          </Button>
        </Paper>
      </Modal>
      <Grid lg={12} container spacing={1}>
        {eventData.length <= 0 ? (
          <Typography
            component="p"
            align="center"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 175px)",
              paddingLeft: "45%",
            }}
          >
            <CircularProgress style={{ color: "black" }} />
          </Typography>
        ) : (
          eventData.map((item, i) =>
            item.message ? (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "calc(100vh - 175px)",
                  paddingLeft: "36%",
                }}
              >
                <h1 style={{ color: "#d3d3d3" }}>{`${item.message}`}</h1>
              </div>
            ) : (
              <Grid
                lg={12}
                key={i}
                item
                xs={12}
                sm={4}
                style={{ maxWidth: "300px", margin: "10px auto" }}
              >
                <motion.div whileHover={{ scale: 0.95 }}>
                  <Card
                    onClick={() => {
                      user.role === 1
                        ? history.push(`/album`, {
                            id: item._id,
                            name: item.name,
                          })
                        : history.push("/photographer/album", {
                            id: item._id,
                            name: item.name,
                          });
                    }}
                  >
                    <CardHeader title={item.name} />
                    <CardContent>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {item.description}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {moment(item.eventDate).format("YYYY-MM-DD")}
                      </Typography>
                      {user.role === 1 ? (
                        <CardActions>
                          <EditIcon
                            onClick={(e) => {
                              e.stopPropagation();
                              setChangeMsg(1);
                              setEventIdData(item._id);
                              handleOpen(
                                item._id,
                                item.description,
                                item.eventDate,
                                item.host,
                                item.photographer,
                                item.eventVenue,
                                item.name
                              );
                            }}
                          />
                          <DeleteIcon
                            onClick={(e) => {
                              handleOpenDeleteEvent();
                              e.stopPropagation();
                              setEventIdData(item._id);
                              // deleteEvent(item._id);
                            }}
                          />
                        </CardActions>
                      ) : (
                        ""
                      )}
                    </CardContent>
                  </Card>
                </motion.div>
              </Grid>
            )
          )
        )}
      </Grid>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={success}
        message={message}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      />
      <Dialog
        open={openDeleteEvent}
        onClose={handleCloseDeleteEvent}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure want to delete the Event?"}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseDeleteEvent}>Disagree</Button>
          <Button onClick={handleCloseDeleteEventAgree} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
