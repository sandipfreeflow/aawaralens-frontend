import React, { useEffect, useState } from "react";
import {
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import { useStyles } from "../BodyStyles";

import { PageHeader } from "../../../Common/Components";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";
import { userCount, eventCount,getAllFilesCount } from "./helper/index";

export default function Dashboard() {
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = useState(false);
  const [totUser, setTotUser] = useState();
  const [totEvent, setTotEvent] = useState();
  const [totFiles, setTotFiles] = useState();



  useEffect(() => {
    const dashboardFetchData = async () => {

      const resUserCount = await userCount()
      const resEventCount = await eventCount()
      const resFilesCount = await getAllFilesCount()

      setTotUser(resUserCount.userCount);
      setTotEvent(resEventCount.eventCount)
      setTotFiles(resFilesCount)
    }
    dashboardFetchData()
  }, [])

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };

  const DisplayData = [
    {
      label: "Events",
      value: `${totEvent}`,
      link: "/events"

    },
    {
      label: "Gallery",
      value: `${totFiles}`,
      link: "/gallery"
    },
    {
      label: "Users",
      value: `${totUser}`,
      link: "/users"

    },

  ];

  return (
    <>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <Box mt={2}>
        {/* //title section  */}
        <PageHeader label='Dashboard' title='Overview' />

        <Grid container spacing={6} className={classes.section}>
          {DisplayData.map((item, i) => (
            <Grid key={i} item xs={6} sm={3} md={3}>
              <Link to={item && item.link} style={{ textDecoration: 'none' }}>
                <motion.div
                  whileHover={{ scale: 0.9 }}
                  initial={{ opacity: 1, scale: 0.6 }} animate={{ opacity: 1, scale: 1 }}
                >
                  <Card style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                    <CardContent className={classes.displayCard}>

                      <Box className={classes.cardDataContent}>
                        <Typography
                          variant='subtitle2'
                          className={classes.cardLabel}
                          gutterBottom={true}>
                          {item.label}
                        </Typography>
                        <Typography
                          variant='h4'
                          component='h2'
                          className={classes.cardHeader}>
                          {item.value === "undefined" ? <CircularProgress style={{ color: "black" }} /> : item.value}
                        </Typography>
                      </Box>
                    </CardContent>
                  </Card>
                </motion.div>
              </Link>
            </Grid>
          ))}

        </Grid>
      </Box>
    </>
  );
}
