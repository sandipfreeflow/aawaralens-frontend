import React, { useState, useEffect } from "react";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import FullscreenIcon from "@material-ui/icons/Fullscreen";
import Lightbox from "react-image-lightbox";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import "react-image-lightbox/style.css";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {
  getAllFilesByAlbumId,
  getAllFilesByAlbumIdCount,
  fileDetailsFormEditData,
  deleteImage,
} from "./helper/index";
import { PageHeader } from "../../../Common/Components";
import Modal from "@material-ui/core/Modal";
import AddIcon from "@material-ui/icons/Add";
import { TextField, Paper, FormHelperText } from "@material-ui/core";
import {
  CircularProgress,
  Fab,
  Hidden,
  Snackbar,
  Dialog,
  DialogActions,
  DialogTitle,
} from "@material-ui/core";
import { motion } from "framer-motion";
import { isAutheticated } from "../Auth/helper/index";
import "./GalleryStyles.css";
import "./AlbumImages.css";
import Axios from "axios";
import CloseIcon from "@material-ui/icons/Close";
import DeleteIcon from "@material-ui/icons/Delete";
import Masonry from "react-masonry-css";
import moment from "moment";
import { getOrientation, resetOrientation } from "./exif-rotator";

const useStyles = makeStyles((theme) => ({
  addButton: {
    position: "fixed",
    right: 12,
    bottom: 60,
    zIndex: 99,
  },
}));

export default function AlbumImages(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    vertical: "top",
    horizontal: "right",
  });
  const { vertical, horizontal } = state;
  const { user } = isAutheticated();

  const [mobileOpen, setMobileOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const [open, setOpen] = useState(false);
  const handleClose = () => {
    setOpen(false);
    setImgError(false);
    setProcessingPercentage(0);
    setUploadPercentage(0);
    setImgError(false);
  };
  const handleCloseImgDetails = () => {
    setValues({ ...values, imagename: "", caption: "" });
    setViewImgDetails(false);
  };
  const [openDeleteImage, setOpenDeleteImage] = useState(false);
  const [imgError, setImgError] = useState(false);
  const [imageLoaded, setImageLoaded] = useState(false);
  const [dispForm, setDispForm] = useState(1);
  const [loader, setLoader] = useState(false);
  const [message, setMessage] = useState("");
  const [success, setSuccess] = useState(false);
  const [fileData, setfileData] = useState([]);
  const [viewImgDetails, setViewImgDetails] = useState(false);
  const [indexData, setIndexData] = useState();
  const [photoIndex, setPhotoIndex] = useState(0);
  const [processingPercentage, setProcessingPercentage] = useState(0);
  const [uploadPercentage, setUploadPercentage] = useState(0);
  const [uploadedImage, setUploadedImage] = useState(0);
  const [totalImgCount, setTotalImgCount] = useState(0);
  const [ImgCount, setImgCount] = useState("");
  const [fileId, setFileId] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  const [values, setValues] = useState({
    name: `${props.location.state.name}`,
    albumVenue: `${props.location.state.albumVenue}`,
    albumDate: `${props.location.state.albumDate}`,
    caption: "",
    albumid: `${props.location.state.id}`,
    eventid: `${props.location.state.eevent}`,
    photo: "",
    uploaderid: `${user._id}`,
    imagename: "",
  });

  const {
    name,
    caption,
    photo,
    eventid,
    uploaderid,
    albumid,
    albumVenue,
    albumDate,
    imagename,
  } = values;

  useEffect(() => {
    const filesFetchData = async () => {
      if (fileData.length === 0) {
        setfileData(await getAllFilesByAlbumId(props.location.state.id));
      }
      if (ImgCount.length === 0) {
        setImgCount(await getAllFilesByAlbumIdCount(props.location.state.id));
      }
    };
    filesFetchData();
  }, [
    fileData.length,
    props.location.state.id,
    processingPercentage,
    uploadPercentage,
    imgError,
    uploadedImage,
    totalImgCount,
    ImgCount,
    fileId,
  ]);
  const deleteFile = async (fileId) => {
    await deleteImage(fileId);
    setfileData(await getAllFilesByAlbumId(props.location.state.id));
    setImgCount(await getAllFilesByAlbumIdCount(props.location.state.id));
    setState({ vertical: "bottom", horizontal: "left" });
    setSuccess(true);
    setMessage("Image deleted successfully");
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleOpenDeleteImage = () => {
    setOpenDeleteImage(true);
  };
  const handleCloseDeleteImage = () => {
    setOpenDeleteImage(false);
  };
  const handleCloseDeleteImageAgree = () => {
    deleteFile(fileId);
    setOpenDeleteImage(false);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };
  const handleCloseSnackbar = (event) => {
    setState({ ...state });
    setSuccess(false);
  };
  const handleChange = (name) => (event) => {
    const value =
      name === "photo"
        ? Object.values(event.target.files).map((data) => data)
        : event.target.value;
    if (value.length === 0) {
      setImgError(true);
    } else {
      value.forEach((item) => {
        if (!item.name.match(/\.(jpg|jpeg|JPG|JPEG)$/)) {
          setImgError(true);
        } else {
          setImgError(false);
        }
      });
      setValues({ ...values, error: false, [name]: value });
    }
  };

  const onSubmit = (event) => {
    if (!imgError) {
      setLoader(true);
      setDispForm(0);
      if (loader === true) {
        <Typography
          component={"span"}
          align="center"
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "calc(100vh - 175px)",
            paddingLeft: "45%",
          }}
        >
          <CircularProgress style={{ color: "black" }} />
        </Typography>;
      }
      event.preventDefault();
      const uploadImages = async (uploadData) => {
        let chunkIndex = 0,
          data,
          uploadedDataProgress = 0,
          totalData;
        const chunk = (arr, n) => {
          const result = [];

          while (arr.length > 0) {
            result.push(arr.splice(0, n));
          }

          return result;
        };
        totalData = uploadData.photo.length;
        setTotalImgCount(totalData);

        const chunkImgData = chunk(uploadData.photo, 5);
        const func = async (index) => {
          if (index < chunkImgData.length) {
            const base64 = chunkImgData[index].map(async (item, i) => {
              const data = new Promise(async (resolve, reject) => {
                const orientation = await getOrientation(item);
                if (orientation > 1) {
                  resolve(await resetOrientation(item, "base64"));
                } else {
                  var reader = new FileReader();
                  reader.readAsDataURL(item);
                  reader.onload = () => {
                    resolve(reader.result);
                  };
                  reader.onerror = reject;
                }
              });
              return await data;
            });
            const imgData = await Promise.all(base64);
            uploadedDataProgress = uploadedDataProgress + imgData.length;
            uploadData.photo = imgData;
            try {
              const response = await Axios({
                method: "post",
                url: "http://192.168.1.161:2087/api",
                data: uploadData,
                headers: {
                  "Content-Type": "application/json",
                },

                onUploadProgress(progressEvent) {
                  const percentCompleted = Math.round(
                    (progressEvent.loaded * 100) / progressEvent.total
                  );
                  setUploadPercentage(percentCompleted);
                },
              });
              setUploadedImage(uploadedDataProgress);
              setProcessingPercentage(
                Math.floor((uploadedDataProgress / totalData) * 100)
              );

              if (response) {
                chunkIndex = chunkIndex + 1;

                if (chunkIndex < chunkImgData.length) {
                  data = await func(chunkIndex);
                }
                return response;
              }
            } catch (err) {
              setState({ vertical: "bottom", horizontal: "left" });
              setSuccess(true);
              setMessage("Error occured while uploading the images");
              setOpen(false);
              setDispForm(1);
            }
          }
        };
        // eslint-disable-next-line no-unused-vars
        return (data = await func(chunkIndex));
      };
      //backend request fired
      uploadImages({
        name,
        albumDate,
        albumVenue,
        caption,
        eventid,
        albumid,
        photo,
        uploaderid,
      }).then(async (data) => {
        if (data) {
          setLoader(true);
          setfileData(await getAllFilesByAlbumId(props.location.state.id));
          setImgCount(await getAllFilesByAlbumIdCount(props.location.state.id));
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage("Files uploaded successfully");
          setOpen(false);
          setDispForm(1);
        } else {
          setState({ vertical: "bottom", horizontal: "left" });
          setSuccess(true);
          setMessage("Error occured while uploading the images");
          setOpen(false);
          setDispForm(1);
        }
      });
    }
  };

  const handleFileDataChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };
  const onSubmitFileData = (event) => {
    event.preventDefault();

    fileDetailsFormEditData(fileId, {
      name,
      albumDate,
      albumVenue,
      caption,
      eventid,
      albumid,
      photo,
      uploaderid,
      imagename,
    }).then(async (data) => {
      if (data.error) {
        setState({ vertical: "bottom", horizontal: "left" });
        setSuccess(true);
        setMessage("Error: Unable to update data");
        handleCloseImgDetails();
      } else {
        setState({ vertical: "bottom", horizontal: "left" });
        setSuccess(true);
        setMessage("Updated Successfully");
        setfileData(await getAllFilesByAlbumId(props.location.state.id));
        handleCloseImgDetails();
      }
    });
  };

  const paperStyle = {
    position: "relative",
    padding: 20,
    minHeight: "30vh",
    height: "auto",
    width: 280,
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
  };
  const viewImgDetailsStyle = {
    position: "relative",
    padding: 10,
    minHeight: "30vh",
    maxHeight: "85vh",
    overflowY: "auto",
    height: "auto",
    width: "60%",
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
    borderRadius: "20px",
  };
  const btnstyle = { margin: "8px 0" };

  const breakpointCols = {
    default: 5,
    1600: 4,
    1300: 3,
    700: 1,
  };
  return (
    <div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <PageHeader
        label="Album Images"
        title={`${props.location.state.name}`}
        countDisp={`Total Image: ${
          ImgCount && ImgCount.resp && ImgCount.resp.data
        }`}
      />
      {fileData.length === 0 ? (
        <Typography
          component={"span"}
          align="center"
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "calc(100vh - 175px)",
          }}
        >
          <CircularProgress style={{ color: "black" }} />
        </Typography>
      ) : (
        <Masonry
          breakpointCols={breakpointCols}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          <Fab
            className={classes.addButton}
            style={{
              backgroundColor: "#212121",
              color: "#FFFFFF",
              height: "80px",
              width: "80px",
            }}
            onClick={handleOpen}
          >
            <AddIcon />
          </Fab>
          {fileData && fileData.resp && fileData.resp.status === 400 ? (
            <div
              style={{
                height: "calc(100vh - 275px)",
                display: "flex",
                alignItems: "center",
                justifyContent: "right",
              }}
            >
              <h1 style={{ color: "#d3d3d3" }}>No data found</h1>
            </div>
          ) : (
            fileData &&
            fileData.resp &&
            fileData.resp.data.map((item, i) => (
              <Grid
                lg={12}
                key={i}
                item
                xs={12}
                sm={4}
                style={{
                  maxWidth: "400px",
                }}
              >
                <motion.div whileHover={{ scale: 0.95 }}>
                  <Card
                    onClick={async () => {
                      setViewImgDetails(true);
                      setPhotoIndex(i);
                      setIndexData(i);
                      setValues({
                        ...values,
                        imagename: `${item.imagename}`,
                        caption: `${item.caption}`,
                      });
                      setFileId(item._id);
                    }}
                    style={{
                      width: "260px",
                      borderRadius: "15px",
                      height: "300px",
                      overflow: "hidden",
                      position: "relative",
                    }}
                  >
                    <div
                      style={{
                        width: "100%",
                        height: "200px",
                        overflow: "hidden",
                        display: "block",
                      }}
                    >
                      <img
                        style={{
                          width: "100%",
                          objectFit: "cover",
                        }}
                        className={`smooth-image image-${
                          imageLoaded ? "visible" : "hidden"
                        }`}
                        onLoad={() => {
                          setImageLoaded(true);
                        }}
                        src={`${item && item.thumbnails3Url}`}
                        alt={`${item.name}`}
                      />
                    </div>
                    {!imageLoaded && (
                      <div className="smooth-preloader">
                        <span className="loader" />
                      </div>
                    )}
                    <CardContent>
                      <div
                        style={{
                          display: "flex",
                          overflow: "hidden",
                          whiteSpace: "nowrap",
                          textOverflow: "ellipsis",
                          width: "300px",
                        }}
                      >
                        <Typography
                          component={"span"}
                          variant="subtitle1"
                          style={{
                            overflow: "hidden",
                            whiteSpace: "nowrap",
                            textOverflow: "ellipsis",
                            width: "200px",
                          }}
                        >
                          Image Name: {`${item.imagename}`}
                        </Typography>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          overflow: "hidden",
                          whiteSpace: "nowrap",
                          textOverflow: "ellipsis",
                          width: "300px",
                        }}
                      >
                        {item &&
                          item.presentuser &&
                          item.presentuser.map((personName) => (
                            <Typography
                              component={"span"}
                              variant="subtitle1"
                              style={{
                                overflow: "hidden",
                                whiteSpace: "nowrap",
                                textOverflow: "ellipsis",
                                width: "80px",
                              }}
                            >
                              {`${personName}`}
                            </Typography>
                          ))}
                      </div>
                    </CardContent>
                    {user.role === 1 ? (
                      <Fab
                        style={{
                          position: "absolute",
                          top: 10,
                          right: 10,
                          boxShadow: "none",
                        }}
                        color="inherit"
                        onClick={(e) => {
                          e.stopPropagation();
                          handleOpenDeleteImage();
                          setFileId(item._id);
                        }}
                        size="small"
                      >
                        <DeleteIcon />
                      </Fab>
                    ) : (
                      ""
                    )}
                  </Card>
                </motion.div>
              </Grid>
            ))
          )}
        </Masonry>
      )}
      <Modal
        open={open}
        onClose={(_, reason) => {
          if (reason !== "backdropClick") {
            handleClose();
          }
        }}
        onKeyDown={() => {}}
        onKeyUp={() => {}}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div>
          {dispForm === 1 ? (
            <Paper elevation={10} style={paperStyle}>
              <CloseIcon
                style={{ position: "absolute", right: 5, top: 5 }}
                onClick={handleClose}
              />
              <Grid lg={12} align="center">
                <h2>Upload Images</h2>
              </Grid>

              <input
                type="file"
                id="files"
                name="files"
                multiple
                required
                accept="image/JPEG,image/JPG, image/jpg, image/jpeg"
                onChange={handleChange("photo")}
              />
              {imgError ? (
                <FormHelperText error>
                  Only jpeg or jpg files are accepted
                </FormHelperText>
              ) : (
                ""
              )}
              {imgError ? (
                <Button
                  type="submit"
                  variant="contained"
                  fullWidth
                  style={{
                    btnstyle,
                    backgroundColor: "#777777",
                    color: "white",
                    marginTop: "30px",
                  }}
                  disabled
                >
                  Submit
                </Button>
              ) : (
                <Button
                  type="submit"
                  variant="contained"
                  fullWidth
                  style={{
                    btnstyle,
                    backgroundColor: "#212121",
                    color: "white",
                    marginTop: "30px",
                  }}
                  onClick={onSubmit}
                >
                  Submit
                </Button>
              )}
            </Paper>
          ) : (
            <Card style={{ borderRadius: "12px" }}>
              {/* <CardHeader title={item.name} /> */}
              <CardContent>
                <Typography
                  component={"span"}
                  variant="h5"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                    height: "300px",
                    color: "black",
                  }}
                >
                  <>
                    Processing Status: {`${processingPercentage}%`}
                    <div
                      style={{
                        background: "#969696",
                        height: "10px",
                        width: "100%",
                        borderRadius: "6px",
                      }}
                    >
                      <motion.div
                        initial={{
                          width: 0,
                        }}
                        animate={{
                          width: `${processingPercentage}%`,
                        }}
                        transition={{ duration: 0.5, delay: 0.1 }}
                        style={{
                          background: "#2B2B2B",
                          height: "10px",
                          borderRadius: "6px",
                        }}
                      />
                    </div>
                  </>
                  <br />
                  <>
                    Uploaded:
                    {`${uploadedImage} out of ${
                      totalImgCount === 0 ? "..." : totalImgCount
                    }`}
                  </>
                </Typography>
              </CardContent>
            </Card>
          )}
        </div>
      </Modal>
      <Modal
        open={viewImgDetails}
        onClose={handleCloseImgDetails}
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div>
          <Paper elevation={10} style={viewImgDetailsStyle}>
            <Hidden only={["lg", "md", "xl"]}>
              <Fab
                style={{ position: "absolute", right: 15, boxShadow: "none" }}
                color="inherit"
                onClick={handleCloseImgDetails}
                size="small"
              >
                <CloseIcon />
              </Fab>
            </Hidden>

            <Hidden only={["sm", "xs"]}>
              <CloseIcon
                style={{ position: "absolute", right: 15 }}
                onClick={handleCloseImgDetails}
              />
            </Hidden>
            <Grid lg={12} container spacing={1}>
              <Grid item lg={8} md={8} sm={12} xs={12}>
                <img
                  style={{
                    width: "80%",
                    height: "80%",
                    borderRadius: "12px",
                    position: "relative",
                    overflow: "hidden",
                    display: "block",
                  }}
                  src={`${
                    fileData &&
                    fileData.resp &&
                    fileData.resp.data &&
                    fileData.resp.data[indexData] &&
                    fileData.resp.data[indexData].compressedphotourl
                  }`}
                  alt={`${
                    fileData &&
                    fileData.resp &&
                    fileData.resp.data &&
                    fileData.resp.data[indexData] &&
                    fileData.resp.data[indexData].compressedphotourl
                  }`}
                />
                <Fab
                  style={{
                    position: "absolute",
                    top: 9,
                    left: 9,
                    boxShadow: "none",
                  }}
                  color="inherit"
                  onClick={() => {
                    handleCloseImgDetails(true);
                    setIsOpen(true);
                  }}
                  size="small"
                >
                  <FullscreenIcon />
                </Fab>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  {fileData &&
                    fileData.resp &&
                    fileData.resp.data &&
                    fileData.resp.data[indexData] &&
                    fileData.resp.data[indexData].presentuser &&
                    fileData.resp.data[indexData].presentuser.map(
                      (personName) => (
                        <Typography
                          component={"span"}
                          variant="subtitle1"
                          style={{ marginLeft: ".5rem" }}
                        >
                          {`${personName}`}
                        </Typography>
                      )
                    )}
                </div>
              </Grid>
              <Grid item lg={4} md={4} sm={12} xs={12}>
                <form style={{ marginTop: "30px" }}>
                  <div className="rightdiv1">
                    <TextField
                      defaultValue={`${
                        fileData &&
                        fileData.resp &&
                        fileData.resp.data &&
                        fileData.resp.data[indexData] &&
                        fileData.resp.data[indexData].imagename
                      }`}
                      label="Image Name"
                      placeholder="Image Name"
                      type="text"
                      required
                      fullWidth
                      onChange={handleFileDataChange("imagename")}
                    />
                  </div>
                  <div className="rightdiv2">
                    <TextField
                      id="standard-multiline-static"
                      label="Caption"
                      multiline
                      rows={4}
                      fullWidth
                      defaultValue={`${
                        fileData &&
                        fileData.resp &&
                        fileData.resp.data &&
                        fileData.resp.data[indexData] &&
                        fileData.resp.data[indexData].caption
                      }`}
                      placeholder="Enter Caption"
                      variant="standard"
                      onChange={handleFileDataChange("caption")}
                    />
                  </div>
                  <div className="rightdiv3">
                    <TextField
                      defaultValue={`${moment(
                        `${
                          fileData &&
                          fileData.resp &&
                          fileData.resp.data &&
                          fileData.resp.data[indexData] &&
                          fileData.resp.data[indexData].capture_date
                        }`
                      )
                        .utc()
                        .format("YYYY-MM-DD")}`}
                      label="Date"
                      placeholder="Date"
                      type="date"
                      required
                      fullWidth
                      disabled
                    />
                  </div>
                  <div className="rightdiv4">
                    <TextField
                      defaultValue={`${values.albumVenue}`}
                      label="Venue"
                      placeholder="Venue"
                      type="text"
                      required
                      fullWidth
                      disabled
                      // onChange={handleFileDataChange("albumVenue")}
                    />
                  </div>
                  <div className="rightdiv5">
                    <Button
                      type="submit"
                      variant="contained"
                      style={{
                        btnstyle,
                        backgroundColor: "#212121",
                        color: "white",
                        marginTop: "30px",
                      }}
                      onClick={onSubmitFileData}
                    >
                      Submit
                    </Button>
                  </div>
                </form>
              </Grid>
            </Grid>
          </Paper>
        </div>
      </Modal>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={success}
        message={message}
        autoHideDuration={10000}
        onClose={handleCloseSnackbar}
      />
      <Dialog
        open={openDeleteImage}
        onClose={handleCloseDeleteImage}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure want to delete the Image?"}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseDeleteImage}>Disagree</Button>
          <Button onClick={handleCloseDeleteImageAgree} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
      {isOpen && (
        <Lightbox
          mainSrc={
            fileData &&
            fileData.resp &&
            fileData.resp.data &&
            fileData.resp.data[photoIndex] &&
            fileData.resp.data[photoIndex].photoUrl
          }
          nextSrc={
            fileData &&
            fileData.resp &&
            fileData.resp.data &&
            fileData.resp.data[photoIndex + 1] &&
            fileData.resp.data[photoIndex + 1].photoUrl
          }
          prevSrc={
            fileData &&
            fileData.resp &&
            fileData.resp.data &&
            fileData.resp.data[photoIndex - 1] &&
            fileData.resp.data[photoIndex - 1].photoUrl
          }
          onMovePrevRequest={() =>
            setPhotoIndex(
              (photoIndex +
                (fileData &&
                  fileData.resp &&
                  fileData.resp.data &&
                  fileData.resp.data.length) -
                1) %
                (fileData &&
                  fileData.resp &&
                  fileData.resp.data &&
                  fileData.resp.data.length)
            )
          }
          onMoveNextRequest={() =>
            setPhotoIndex((photoIndex + 1) % fileData.resp.data.length)
          }
          onCloseRequest={() => {
            setIsOpen(false);
          }}
        />
      )}
    </div>
  );
}
