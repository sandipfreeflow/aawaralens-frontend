import React, { useState, useEffect } from "react";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { Paper } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { getAllUsers, editUserById } from "./helper/index";
import { isAutheticated } from "../Auth/helper/index";
import {
  CircularProgress,
  CardHeader,
  CardActions,
  Modal,
} from "@material-ui/core";
import { motion } from "framer-motion";
import moment from "moment";
import EditIcon from "@material-ui/icons/Edit";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

export default function Users() {
  const { token } = isAutheticated();
  const [open, setOpen] = useState(false);
  const [userData, setUserData] = useState([]);
  const [userId, setUserId] = useState();
  const handleOpen = (id, roleData) => {
    setUserId(id);
    setOpen(true);
    setRole(roleData);
  };
  const handleClose = () => setOpen(false);
  const [mobileOpen, setMobileOpen] = useState(false);

  useEffect(() => {
    const userFetchData = async () => {
      if (userData.length === 0) {
        setUserData(await getAllUsers());
      }
    };
    userFetchData();
  }, [userData]);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };

  const [role, setRole] = React.useState(0);

  const handleChange = (event) => {
    setRole(event.target.value);
  };
  const onSubmit = (event) => {
    event.preventDefault();
    editUserById(token, userId, { role: role }).then(async (data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        handleClose();
        setUserData(await getAllUsers());
      }
    });
  };

  const paperStyle = {
    padding: 20,
    minHeight: "40vh",
    height: "auto",
    width: 280,
    margin: "20px auto",
    backgroundColor: "#EFEFEF",
  };
  const btnstyle = { margin: "8px 0" };
  return (
    <div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <h1> Users</h1>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Paper elevation={10} style={paperStyle}>
          <Grid lg={12} align="center">
            <h2>Change the Role</h2>
          </Grid>
          <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
            <InputLabel id="demo-simple-select-standard-label">Role</InputLabel>
            <Select
              labelId="demo-simple-select-standard-label"
              id="demo-simple-select-standard"
              value={role}
              onChange={handleChange}
              label="Role"
              focused
            >
              <MenuItem value={1}>Admin</MenuItem>
              <MenuItem value={0}>User</MenuItem>
              <MenuItem value={2}>Photographer</MenuItem>
            </Select>
          </FormControl>

          <Button
            type="submit"
            variant="contained"
            fullWidth
            style={{
              btnstyle,
              backgroundColor: "#212121",
              color: "white",
              marginTop: "30px",
            }}
            onClick={onSubmit}
          >
            Submit
          </Button>
        </Paper>
      </Modal>
      <Grid lg={12} container spacing={1}>
        {userData.length <= 0 ? (
          <Typography
            component="p"
            align="center"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "calc(100vh - 175px)",
              paddingLeft: "45%",
            }}
          >
            <CircularProgress style={{ color: "black" }} />
          </Typography>
        ) : (
          userData.map((item, i) => (
            <Grid
              lg={12}
              key={i}
              item
              xs={12}
              sm={4}
              style={{ maxWidth: "300px", margin: "10px auto" }}
            >
              <motion.div whileHover={{ scale: 0.95 }}>
                <Card style={{ borderRadius: "15px" }}>
                  <CardHeader
                    title={
                      (item.firstname || item.name) +
                      " " +
                      (item.lastname || " ")
                    }
                  />
                  <CardContent>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      {item.email}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      {item.phone}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      {item.role <= 0
                        ? "User"
                        : item.role === 1
                        ? "Admin"
                        : "Photographer"}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      {moment(item.createdAt).format("YYYY-MM-DD")}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <EditIcon
                      onClick={(e) => {
                        e.stopPropagation();
                        handleOpen(item._id, item.role);
                      }}
                    />
                  </CardActions>
                </Card>
              </motion.div>
            </Grid>
          ))
        )}
      </Grid>
    </div>
  );
}
