import React, { useState, useEffect } from "react";
import NavbarComponent from "../../Header/NavbarComponent";
import Sidenav from "../../Header/Sidenav";
import Card from "@material-ui/core/Card";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import Typography from "@material-ui/core/Typography";
import { getAllFiles, getAllImages } from "./helper/index";
import { CircularProgress } from "@material-ui/core";
import { motion } from "framer-motion";
import "./GalleryStyles.css";
import Masonry from 'react-masonry-css'

export default function Gallery() {
  const [galleryImageData, setGalleryImageData] = useState("");
  const [mobileOpen, setMobileOpen] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(0);
  const [isOpen, setIsOpen] = useState(false);
  const [images, setImages] = useState([]);
  const [imageLoaded, setImageLoaded] = useState(false);

  useEffect(() => {
    const albumImageFetchData = async () => {
      if (galleryImageData === "") {
        setGalleryImageData(await getAllFiles());
      }
    };
    const photoUrlData = async () => {
      if (images.length === 0) {
        setImages(await getAllImages());
      }
    };
    albumImageFetchData();
    photoUrlData();
  }, [galleryImageData, images]);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleDrawerClose = () => {
    setMobileOpen(false);
  };
  const breakpointCols = {
    default: 5,
    1600: 4,
    1300: 3,
    700: 1,
  };
  return (
    <div>
      <NavbarComponent handleDrawerToggle={handleDrawerToggle} />
      <Sidenav
        mobileOpen={mobileOpen}
        handleDrawerClose={handleDrawerClose}
        handleDrawerToggle={handleDrawerToggle}
      />
      <h1> Gallery</h1>
      
      {galleryImageData === "" ? (
        <Typography
        component={'span'}
          align="center"
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "calc(100vh - 185px)",
          }}
        >
          <CircularProgress style={{ color: "black" }} />
        </Typography>
      ) : (
        <Masonry
          breakpointCols={breakpointCols}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column">
          {galleryImageData &&
            galleryImageData.map((item, i) => (
              <div>
                <div>
                  <motion.div whileHover={{ scale: 0.95 }}>
                    <Card
                      onClick={() => {
                        setIsOpen(true);
                        setPhotoIndex(i);
                      }}
                      style={{
                        width: "260px",
                        borderRadius: "15px",
                        height: "200px",
                        overflow: "hidden",
                        position: "relative",
                      }}
                    >
                      <div
                        style={{
                          width: "100%",
                          height: "200px",
                          overflow: "hidden",
                          display: "block",
                        }}
                      >
                        <img
                          style={{ width: "100%", minHeight: "200px" }}
                          className={`smooth-image image-${imageLoaded ? "visible" : "hidden"
                            }`}
                          onLoad={() => {
                            setImageLoaded(true);
                          }}
                          src={`${item.thumbnails3Url}`}
                          alt="picture123"
                        />
                        {!imageLoaded && (
                          <div className="smooth-preloader">
                            <span className="loader" />
                          </div>
                        )}
                        </div>
                    </Card>
                  </motion.div>
                </div>
              </div>
            ))
          }
        </Masonry>
      )
      }

      {isOpen &&
        (
          (
            <Lightbox
              mainSrc={images[photoIndex]}
              nextSrc={images[(photoIndex + 1) % images.length]}
              prevSrc={images[(photoIndex + images.length - 1) % images.length]}
              onCloseRequest={() => setIsOpen(false)}
              onMovePrevRequest={() =>
                setPhotoIndex((photoIndex + images.length - 1) % images.length)
              }
              onMoveNextRequest={() =>
                setPhotoIndex((photoIndex + 1) % images.length)
              }
            >
            </Lightbox>
          ))}
    </div>
  );
}
