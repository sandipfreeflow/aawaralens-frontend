import React from "react";
import { Box, Grid, Typography } from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { red } from "@material-ui/core/colors";
import { useStyles } from "./BodyComponent/BodyStyles";

export default function Footer() {
  const classes = useStyles();
  const date = new Date();

  return (
    <Box className={classes.footerLogin}>
      <Typography variant='body1' color='textSecondary' align='center'>
        All right reserved @Aawara Lens {date.getFullYear()}
      </Typography>

    </Box>
  );
}
