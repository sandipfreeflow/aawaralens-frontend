import React from "react";
import { Box, Typography } from "@material-ui/core";
import { useStyles } from "./BodyComponent/BodyStyles";

export default function Footer() {
  const classes = useStyles();
  const date = new Date();

  return (
    <Box className={classes.footer}>
      <Typography variant="body1" color="textSecondary" align="center">
        All right reserved @My Snaps {date.getFullYear()}
      </Typography>
    </Box>
  );
}
