import React from "react";
import {
  Button,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import { NavLink } from "react-router-dom";
import { useStyles } from "./HeaderStyle";

import DashboardIcon from "@material-ui/icons/Dashboard";
import PersonIcon from "@material-ui/icons/Person";
import ImageSearchIcon from '@material-ui/icons/ImageSearch';
import PostAddIcon from "@material-ui/icons/PostAdd";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import ImageIcon from '@material-ui/icons/Image';

export default function SidenavData({ handleDrawerClose }) {
  const classes = useStyles();

  const adminlistItemData = [
    { label: "Dashboard", link: "/admin/dashboard", icon: <DashboardIcon /> },
    { label: "Events", link: "/events", icon: <PostAddIcon /> },
    { label: "Users", link: "/users", icon: <PersonIcon /> },
    { label: "Gallery", link: "/gallery", icon: <ImageIcon /> },
    { label: "logout", link: "/logout", icon: <ExitToAppIcon /> },
  ];
  const userlistItemData = [
    { label: "View your image Event wise", link: "/user/events", icon: <PostAddIcon /> },
    { label: "View all of your images", link: "/user/totalimages", icon: <ImageSearchIcon /> },
    { label: "logout", link: "/logout", icon: <ExitToAppIcon /> },
  ];
  const photographerlistItemData = [
    { label: "Events", link: "/photographer/events", icon: <DashboardIcon /> },
    { label: "logout", link: "/logout", icon: <ExitToAppIcon /> },
  ];
  const { user } = JSON.parse(localStorage.getItem("jwt"));

  return (
    <div>
      {user.role === 1 ?
        adminlistItemData.map((item, i) => (
          <List key={i}>
            <Button
              size='small'
              onClick={() => handleDrawerClose()}
              className={classes.navButton}>
              < ListItem
                exact
                key={i}
                component={NavLink}
                to={item.link}
                className={classes.navlink}
                activeClassName={classes.selectedNav}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText>{item.label}</ListItemText>
              </ListItem>
            </Button>
          </List >
        ))
        :
        user.role === 0 ?
          userlistItemData.map((item, i) => (
            <List  key={i}>
              <Button
                size='small'
                onClick={() => handleDrawerClose()}
                className={classes.navButton}>
                < ListItem
                  exact
                  key={i}
                  component={NavLink}
                  to={item.link}
                  className={classes.navlink}
                  activeClassName={classes.selectedNav}>
                  <ListItemIcon>{item.icon}</ListItemIcon>
                  <ListItemText>{item.label}</ListItemText>
                </ListItem>
              </Button>
            </List >
          ))
          :
          photographerlistItemData.map((item, i) => (
            <List  key={i}>
              <Button
                size='small'
                onClick={() => handleDrawerClose()}
                className={classes.navButton}>
                < ListItem
                  exact
                  key={i}
                  component={NavLink}
                  to={item.link}
                  className={classes.navlink}
                  activeClassName={classes.selectedNav}>
                  <ListItemIcon>{item.icon}</ListItemIcon>
                  <ListItemText>{item.label}</ListItemText>
                </ListItem>
              </Button>
            </List >
          ))

      }

      {/* {user.role === 1 ?
        console.log("Admin logged in") :
        user.role === 0 ?
          console.log("user logged in") :
          console.log("Photographer logged in")
      } */}
    </div>
  );
}
