import React, { Fragment } from "react";
import { Box } from "@material-ui/core";
import { Route, Switch } from "react-router-dom";
import { useStyles } from "./HeaderStyle";
import Users from "../BodyComponent/AdminPages/Users";
import Dashboard from "../BodyComponent/AdminPages/Dashboard";
import Events from "../BodyComponent/AdminPages/Events";
import Signup from "../BodyComponent/Auth/Signup";
import Signin from "../BodyComponent/Auth/Signin";
import Logout from "../BodyComponent/Auth/Logout";
import AdminRoute from "../BodyComponent/Auth/helper/AdminRoutes";
import PhotographerRoutes from "../BodyComponent/Auth/helper/PhotographerRoutes"
import UserRoutes from "../BodyComponent/Auth/helper/UserRoutes";
import ViewAllImages from "../BodyComponent/UserPages/ViewAllImages";
import UserAlbums from "../BodyComponent/UserPages/UserAlbums";
import UserEvents from "../BodyComponent/UserPages/UserEvents";
import Album from "../BodyComponent/AdminPages/Album"
import AlbumImages from "../BodyComponent/AdminPages/AlbumImages";
import Gallery from "../BodyComponent/AdminPages/Gallery";
import ForgotPassword from "../BodyComponent/Auth/ForgotPassword";
import ResetPassword from "../BodyComponent/Auth/ResetPassword"
import ProfilePage from "../BodyComponent/Auth/ProfilePage";
import ChangePassword from "../BodyComponent/Auth/ChangePassword";
import UserAlbumImages from "../BodyComponent/UserPages/UserAlbumImages";

export default function HearderComponent() {
  const classes = useStyles();
  return (
    <>
      <Switch>
        <Route exact path='/' component={Signin} />
        <Route exact path='/signup' component={Signup} />
        <Route exact path='/logout' render={() => <Logout />} />
        <Route exact path='/sendresetpasswordemail' render={() => <ForgotPassword />} />
        <Route exact path='/userPasswordReset/:id/:token' render={() => <ResetPassword />} />
        <Route exact path='/profile' component={ProfilePage} />
        <Route exact path='/changepassword' component={ChangePassword} />


        <Box className={classes.wrapper}>
          <UserRoutes exact path='/user/events' component={UserEvents} />
          <UserRoutes exact path='/user/albumimages' component={UserAlbumImages} />
          <UserRoutes exact path='/user/totalimages' component={ViewAllImages} />
          <UserRoutes exact path='/user/albums' component={UserAlbums} />
          <AdminRoute exact path='/events' component={Events} />
          <AdminRoute exact path='/users' component={Users} />
          <AdminRoute exact path='/admin/dashboard' component={Dashboard} />
          <AdminRoute exact path='/album' component={Album} />
          <AdminRoute exact path='/albumimages' component={AlbumImages} />
          <AdminRoute exact path='/gallery' component={Gallery} />
          <PhotographerRoutes exact path='/photographer/events' component={Events} />
          <PhotographerRoutes exact path='/photographer/album' component={Album} />
          <PhotographerRoutes exact path='/photographer/albumimages' component={AlbumImages} />

        </Box>
      </Switch>
    </>
  );
}
