import React from "react";
import {
  AppBar,
  Box,
  Hidden,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { useStyles } from "./HeaderStyle";
import MenuRoundedIcon from "@material-ui/icons/MenuRounded";

import Profile from "./ActionTab/Profile";

export default function NavbarComponent({ handleDrawerToggle }) {
  const classes = useStyles();
  const name = JSON.parse(localStorage.getItem("jwt"));
  return (
    <AppBar style={{ backgroundColor: "#212121", zIndex: "100" }}>
      <Toolbar className={classes.toolbar}>
        <Box style={{ display: "flex" }}>
          <img
            src="/mysnaps_text.png"
            alt="Aawara Lens Logo"
            width={150}
            style={{ margin: "10px 0px 10px" }}
          />
        </Box>
        <Hidden smDown>
          <Box style={{ display: "flex", alignItems: "center" }}>
            <Typography style={{ fontSize: "1.2rem", margin: "3px 12px 0 0" }}>
              Hi, {`${name && name.user && name.user.firstname}`}
            </Typography>
            <Profile />
          </Box>
        </Hidden>
        <Hidden mdUp>
          <IconButton color="inherit" onClick={handleDrawerToggle}>
            <MenuRoundedIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}
