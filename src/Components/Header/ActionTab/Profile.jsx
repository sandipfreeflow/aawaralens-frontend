import React, { Fragment } from "react";
import {
  Badge,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Menu,
} from "@material-ui/core";

import { useStyles } from "../HeaderStyle";
import image from "./avatar.png";
import SettingsIcon from "@material-ui/icons/Settings";
import { useHistory } from "react-router-dom";

import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import VpnKeyIcon from '@material-ui/icons/VpnKey';

export default function Profile() {
  const history = useHistory();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const redirect = () => {
    localStorage.removeItem("jwt");
    history.push("/");
  };
  const dropDownData = [
    { label: "Profile", link: "/profile", icon: <SettingsIcon /> },
    { label: "Change Password", link: "/changepassword", icon: < VpnKeyIcon /> },
    { label: "logout", link: "/logout", icon: <ExitToAppIcon /> },

  ];
  const userImg = JSON.parse(localStorage.getItem("jwt"));

  return (
    <Fragment>
      <IconButton
        aria-controls="profile"
        aria-haspopup="true"
        onClick={handleClick}
        color="inherit"
      >
        <Badge badgeContent={null} color="secondary">
          <img
            alt={image}
            src={`${userImg.user.photoUrl}`}
            className={classes.navImg}
          />
        </Badge>
      </IconButton>
      <Menu
        id="profile"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        placement="bottom-start"
        style={{ top: "40px" }}
      >
        <List dense={true} className={classes.dropdownlist}>
          {dropDownData.map((item, i) => (
            <ListItem
              exact
              key={i}
              component={Button}
              onClick={() => {
                item.label === "logout" ? redirect() : item.label === "Profile" ? history.push("/profile") : history.push("/changepassword");
              }}
              className={classes.listItem}
            >
              <ListItemAvatar>{item.icon}</ListItemAvatar>
              <ListItemText primary={item.label}></ListItemText>
            </ListItem>
          ))}
        </List>
      </Menu>
    </Fragment>
  );
}
