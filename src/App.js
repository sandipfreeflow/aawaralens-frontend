import { BrowserRouter } from "react-router-dom";
import "./App.css";
import Router from "./Components/Header/Router";
import { API } from "../src/backend";


function App() {
  // console.log(API)
  return (
    <BrowserRouter>
      <Router />
    </BrowserRouter>
  );
}

export default App;
